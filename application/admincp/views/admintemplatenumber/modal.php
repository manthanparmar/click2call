<?php

if ($mode == 'type') {

}elseif ($mode == 'status') {
    
    $modal = '<div style="color:#484848;" class="modal fade" id="statusmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                        <h4 class="modal-title" id="confirm_status_title">Change Status</h4>
                    </div>
                     <form action="' . base_url('admintemplatenumber/changestatus') . '" accept-charset="utf-8" class="form-horizontal" id="frmdesti" method="post" enctype="multipart/form-data">
                    <div class="modal-body" id="confirm_status_body">  
                        <div class="">
                        <div class="" style="margin-bottom:10px !important">
                            
  <label class=""><input type="radio" class="" name="templatenumber_active" value="Enable" ';
        if ($type_status == 'Enable') {
            $modal .= 'checked=""';
        } $modal .='>&nbsp;Enable</label>
            
<label class=""><input type="radio" class="" name="templatenumber_active" value="Disable" ';
        if ($type_status == 'Disable') {
            $modal .= 'checked=""';
        } $modal .='>&nbsp;Disable</label>
           
                          </div>
                        </div>    
                       
                        <input type="hidden" name="templatenumber_id" id="templatenumber_id" value="' . $id . '">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" id="status_submit" class="btn btn-default danger" >Change Status</button>
                        <!--                <a href="#" class="btn btn-default danger">Yes</a>-->
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>';
        echo $modal;
    
}elseif ($mode == 'delete') {
   
   
    $modal = '<div style="color:#484848;" class="modal fade" id="statusmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                        <h4 class="modal-title" id="confirm_status_title">Delete Template number</h4>
                    </div>
                     <form action="' . base_url('admintemplatenumber/delete/'.$sid) . '" accept-charset="utf-8" class="form-horizontal" id="frmdesti" method="post" enctype="multipart/form-data">
                    <div class="modal-body text-muted" id="">  
                        <p class="text-muted "><h4> Are you sure you want to delete ? </h4></p>               
                        <input type="hidden" name="act_id" id="act_id" value="' . $sid . '">
                        
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" id="status_submit" class="btn btn-danger" >Yes</button>

                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>';
        echo $modal;
}


elseif ($mode == 'geturl') {
    
    //$url = 'http://click2call.mobi/template/preview/' .$id.'/'.$sid.'/'.$actid;
     $url = base_url().'../template/preview/' .$id.'/'.$sid.'/'.$actid;
     $modal = '<div id="statusmodel" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Template Url</h4>
                  </div>
                  <div class="modal-body">
                    <input type="text" class="form-control" id="copy" value="'.$url.'" readonly />
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" onclick = "copyurl()" class="btn btn-primary" data-dismiss="modal">Copy</button>
                  </div>
                </div>
            
              </div>
            </div>';
    echo $modal;
}

elseif ($mode == 'getshorturl')
{
  
    $shrturl = base_url().'../template/'.$id;
    // $shrturl = 'http://click2call.mobi/template/'.$id;
    
     $modal = '<div id="statusmodel" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <input type="hidden" value="'.$shrturl.'" />
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Template Url</h4>
                  </div>
                  <div class="modal-body">
                    <input type="text" class="form-control" id="copyshorturl" value="'.$shrturl.'" readonly />
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" onclick = "copyshorturl()" class="btn btn-primary" data-dismiss="modal">Copy</button>
                  </div>
                </div>
            
              </div>
            </div>';
    echo $modal;


}

?>


