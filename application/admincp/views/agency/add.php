<?php echo $header; ?>
<?php echo $leftmenu; ?>

<style type="text/css">
    #payout_record
    {
        padding-left: 0px !important;
    }
    .payout-div
    {
        padding-left: 0px !important;
    }
</style>
<!-- Content Wrapper. Contains agency content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?> Client <!-- <?php echo ucfirst($this->uri->segment(1)); ?> -->
            <small></small>
        </h2>
        <ol class="breadcrumb pull-left">
            <li class="pull-left"><a title="Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a title="Client"><i class="fa fa-university"></i> Client<!-- <?php echo ucfirst($this->uri->segment(1)); ?> --></a></li>
            <!--<?php if ($this->uri->segment(2))
            { ?><li><a href="<?php echo base_url($this->uri->segment(3)); ?>" title="Add"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>-->
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
<?php if ($this->session->flashdata('message'))
{ ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
<?php } ?>
<?php if ($this->session->flashdata('success'))
{ ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
<?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php
                    $attributes = array('class' => 'has-validation-callback', 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('agency/addagency/', $attributes);
                    ?>

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Client Name</label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="agencyname" data-validation="required" data-validation-error-msg="Please enter agency name." id="agencyname" type="text" class="form-control" value="" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Client Email</label>&nbsp<span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="agencyemail" data-validation="email required server" data-validation-url="email_exist" data-validation-error-msg="Please enter valid email." id="agencyemail" type="text" class="form-control" value="" />
                                        </div>
                                    </div> 

                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Client Password</label>&nbsp<span style="color: #a94442;">*</span>
                                        <div class="controls">
                                        <input name="agencypassword" data-validation="required length"
                                         data-validation-regexp="^(?=.*[A-Z])(?=.*\d).+$" data-validation-error-msg="Password must be 6 to 16 characters long and it should contain at least one character and one digit." data-validation-length="6-16" id="agencypassword" type="password" class="form-control" value="" />
                                                                            
                                        </div>
                                    </div> 
                                </div>
                                
                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>API Username </label>&nbsp<span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="api_username" data-validation="required" data-validation-error-msg="Please enter Api Username." id="api_username" type="text" class="form-control" value="" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>API Password </label>&nbsp<span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="api_password" data-validation="required" data-validation-error-msg="Please enter Api Password." id="api_password" type="text" class="form-control" value="" />
                                        </div>
                                    </div>
                                </div>

                             <!--   <div class="row">
                                     <div id="payout_record" class="col-md-11">
                                        <div class="col-md-12 payout-div" id="row_0">
                                            <div class="form-group col-md-3" id="firstnameerror">
                                                <label>Country</label>&nbsp;
                                                <span style="color: #a94442;">*</span>
                                                <div class="controls">
                                                    <input name="payout_country[]" id="pay_county" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-suggestion-nr="1" data-validation="custom required" data-validation-error-msg="Please enter country." type="text" class="form-control suggestions-1 error paycountry">
                                                    </div>
                                            </div>
                                            <div class="form-group col-md-3" id="firstnameerror">
                                                <label>Call</label>&nbsp;
                                                <span style="color: #a94442;">*</span>
                                                <div class="controls">
                                                    <input name="payout_call[]" data-validation="custom" data-validation-regexp="^[0-9]*.*[0-9]$" data-validation-error-msg="Please enter call price." type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3" id="firstnameerror">
                                                <label>SMS</label>&nbsp;
                                                <span style="color: #a94442;">*</span>
                                                <div class="controls">
                                                    <input name="payout_sms[]" data-validation-regexp="^[0-9]*.*[0-9]$" data-validation="custom" data-validation-error-msg="Please enter sms price." type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-1" id="firstnameerror">
                                                <label>&nbsp;</label>
                                                <div class="controls">
                                                    <span title="Remove" onclick="remove_div('row_0')" id="remove" class="btn btn-danger btn-small">
                                                        <i class="fa fa-remove"></i>
                                                    </span>
                                                </div>
                                            </div>
                                          </div>  
                                     </div>
                                </div>-->

                                <!-- <div class="row">
                                    <div class="form-group col-md-4">
                                        <div class="controls">
                                            <span title="Add new record" onclick="add()" class="btn btn-primary btn-group btn-group-justified" id="add_attr"><i class="fa fa-plus"> Add New Record</i></span>
                                        </div>
                                    </div>
                                </div>-->
                            </div>

                         
                            <script>
                                $.validate({
                                    modules: 'location, date, security, file',
                                    onModulesLoaded: function () {
                                    $('.paycountry').suggestCountry();},
                                });
                            </script>
                        </div><!-- /.box-body -->
                        <div class="col-md-12">
                            <div class="box-footer" style="padding-left:0px">
                                <input type="submit" title="Add" name="add" value="Add" id="add" class="btn btn-primary btn-small" />
                                <input type="button" title="Back" class="btn btn-default btn-small" value="Back" onclick="window.location.href = '<?php echo site_url('agency'); ?>'"/>
                                 <input type="hidden" name="div_count" id="div_count" value="1">
                            </div>
                        </div>
<?php echo form_close(); ?> 
                    </div><!-- /.box -->

                </div><!--/.col (left) -->
            </div>   <!-- /.row -->

        </div>
    </section>
<?php echo $footer; ?>
    <script>
        $("#add").keypress(function (event) {
            if (event.which == 13) {
                return false;
            }

        });
    </script>
  

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="<?php echo base_url(); ?>newadmin/dist/js/jquery.geocomplete.min.js" type="text/javascript"></script>


    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#view_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    
    
    <script>
     $(document).on('mouseover', '.paycountry', function() {
         $(this).suggestCountry();
     });
    </script>
    

 <!--    <script type="text/javascript">
    function remove_div(id)
            {
                $('#' + id).remove();
            }
            
            function add()
            {

                var div_count = $('#div_count').val();
                var div_id = 'row_' + div_count;
             
                var payout_div = '<div class="col-md-12 payout-div" id="row_' + div_count + '">' +
                        '<div class="form-group col-md-3" id="firstnameerror">' +
                        '<label>Country</label>&nbsp<span style="color: #a94442;">*</span>' +
                        '<div class="controls">' +
                        '<input name="payout_country[]" data-validation-suggestion-nr="1" data-validation="custom required"  data-validation-error-msg="Please enter country."  type="text" class="form-control suggestions-1 error paycountry"  />' +
                        '</div>' +
                        '</div>' +


                        
                         '<div class="form-group col-md-3" id="firstnameerror">' +
                        '<label>Call</label>&nbsp<span style="color: #a94442;">*</span>' +
                        '<div class="controls">' +
                        '<input name="payout_call[]" data-validation="custom" data-validation-regexp="^[0-9]*.*[0-9]$" data-validation-error-msg="Please enter call price."  type="text" class="form-control"  />' +
                        '</div>' +
                        '</div>' +



                        '<div class="form-group col-md-3" id="firstnameerror">' +
                        '<label>SMS</label>&nbsp<span style="color: #a94442;">*</span>' +
                        '<div class="controls">' +
                        '<input name="payout_sms[]" data-validation="custom" data-validation-regexp="^[0-9]*.*[0-9]$" data-validation-error-msg="Please enter sms price."  type="text" class="form-control"  />' +
                        '</div>' +
                        '</div>' +


                        '<div class="form-group col-md-1" id="firstnameerror">' +
                        '<label>&nbsp;</label>' +
                        '<div class="controls">' +
                        "<span title='Remove' onclick='remove_div(" + '"' + div_id + '"' + ")' id='remove' class='btn btn-danger btn-small'><i class='fa fa-remove'></i></span>" +
                        '</div>' +
                        '</div>' +
                        '</div>';
                        
                        $("#payout_record").append(payout_div);
                      
                        div_count = parseInt(div_count) + parseInt('1');
                        $('#div_count').val(div_count);
                        
                          $.validate({
                            modules: 'location, date, security, file',
                            onModulesLoaded: function () {
                            }
                        });

            }

</script> -->