<?php echo $header; ?>
<?php echo $leftmenu; ?>

<!-- Content Wrapper. Contains agencytemplate content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?> Client Template
            <!--<?php echo ucfirst($this->uri->segment(1)); ?>-->
            <small></small>
        </h2>
        <ol class="breadcrumb pull-left">
            <li class="pull-left"><a title= "Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a title="Client template"><i class="fa fa-bars"></i> Client template </a></li>
            <!--<?php if ($this->uri->segment(2))
            { ?><li><a title="<?php echo ucfirst($this->uri->segment(2)); ?>" href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>-->
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
<?php if ($this->session->flashdata('message'))
{ ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
<?php } ?>
<?php if ($this->session->flashdata('success'))
{ ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
<?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php
                    $attributes = array('class' => 'has-validation-callback', 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('agencytemplate/addagencytemplate/', $attributes);
                    ?>

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-10">
                                <div class="row">
                                      <div class="form-group col-md-6" id="firstnameerror">
                                        <label>Select Client</label>&nbsp <span style="color: #a94442;">*</span>
                                            <div class="controls" style="width:599px;">
                                                <select name="agency_id" class="form-control" data-validation="required" data-validation-error-msg="Please select agency.">
                                                    <option value="">Select type</option>
                                                   <?php 
                                                    foreach ($agency_data as $agency) { ?>
                                                        <option value="<?php echo $agency['agency_id'];?>"><?php echo $agency['agency_name'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-md-6" id="firstnameerror">
                                            <label>Select Templates</label>&nbsp <span style="color: #a94442;">*</span>
                                            <select id="template_ids" data-validation="required" data-validation-error-msg="Please select template." name = "template_ids[]" multiple="multiple" style="width: 600px">
                                                <option value="">-- Select templates --</option>
                                                <?php 
                                                    foreach ($template_data as $template) { ?>
                                                    <option value="<?php echo $template['template_id']; ?>"><?php echo $template['template_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                </div>

                            </div>

                         
                            <script>

                                $.validate({
                                    modules: 'location, date, security, file',
                                    onModulesLoaded: function () {
                                    },
                                });

                            </script>
                        </div><!-- /.box-body -->
                        <div class="col-md-12">
                            <div class="box-footer" style="padding-left:0px;">
                                <input type="submit" title="Add" name="add" value="Add" id="add" class="btn btn-primary btn-small" />
                                <input type="button" title="Back" class="btn btn-default btn-small" value="Back" onclick="window.location.href = '<?php echo site_url('agencytemplate'); ?>'"/>
                            </div>
                        </div>
<?php echo form_close(); ?> 
                    </div><!-- /.box -->

                </div><!--/.col (left) -->
            </div>   <!-- /.row -->

        </div>
    </section>
<?php echo $footer; ?>
    <script>
        $("#add").keypress(function (event) {
            if (event.which == 13) {
                return false;
            }

        });
    </script>
    
    <script>
    $('#template_ids').select2({
        placeholder: 'Select a templates'
    });
    </script>

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="<?php echo base_url(); ?>newadmin/dist/js/jquery.geocomplete.min.js" type="text/javascript"></script>


    <script>
        // $('#agency_numbers_list').multiSelect();

        $("#street").geocomplete({
            details: "#details",
            detailsAttribute: "data-geo",
            types: ["geocode", "establishment"]
        });
        function set_value()
        {
            setTimeout(function () {
                var country = $('#address_country1').val();
                var city = $('#address_city1').val();
                var zipcode = $('#address_zipcode1').val();
                $('#country').val(country);
                $('#city').val(city);
                $('#zipcode').val(zipcode);

            }, 1000);

        }
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#view_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>