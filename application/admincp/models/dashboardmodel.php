<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboardmodel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    // New function for filter data 25/02/2019

    public function get_history_filter_data($from_date,$to_date,$agency_id,$calltype)
    {
        $this->db->select('csh.*,nb.payout,nb.currency,nb.number');
        $this->db->from('call_sms_history csh');
        $this->db->join('number nb','nb.number_id = csh.from_numberid');
        if($from_date != "")
        {
            $date = $from_date;
            $res = explode("-", $date);
            $changedDate = $res[2]."-".$res[0]."-".$res[1];
            $this->db->where('DATE(csh.history_date) >=',$changedDate);
        }
        if($to_date != "")
        {
            $date = $to_date;
             $res = explode("-", $date);
            $changedDate = $res[2]."-".$res[0]."-".$res[1];
            $this->db->where('DATE(csh.history_date) <=',$changedDate);
        }
        if($agency_id != "")
        {
            $this->db->where('csh.agency_id',$agency_id);
        }
        if($calltype != "")
        {
            $this->db->where('csh.history_type',$calltype);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function total_agency()
    {
        $this->db->select('*');
        $this->db->where('agency_active !=','Delete');
        $query = $this->db->get('agency');
        return $query->result_array();
    }

    public function userGetAllMember()
    {
        return $this->db->count_all('user');
    }

    public function userCountByPeriod($condition = NULL)
    {

        $this->db->select('count(user_id) as status');
        $this->db->from('user');

        if ($condition == 'lastmonth')
        {
            $this->db->where('signupdate > (NOW() - INTERVAL 1 MONTH) ;');
        }
        elseif ($condition == 'today')
        {
            $this->db->where('YEAR(signupdate) = YEAR(NOW()) AND MONTH(signupdate) = MONTH(NOW()) AND DAY(signupdate) = DAY(NOW());');
        }
        elseif ($condition == 'thisyear')
        {
            $this->db->where('YEAR(signupdate) = YEAR(CURDATE());');
        }
        else
        {
            return FALSE;
        }

        return $this->db->get()->row();
    }

    public function userCountByType($type = NULL)
    {

        $this->db->select('count(user_id) as type');
        $this->db->from('user');

        if ($type == 'user')
        {
            $this->db->where(array('type' => 'user'));
        }
        elseif ($type == 'driver')
        {
            $this->db->where(array('type' => 'driver'));
        }
        elseif ($type == 'toursguide')
        {
            $this->db->where(array('type' => 'tours guide'));
        }
        else
        {
            return FALSE;
        }
        return $this->db->get()->row();
    }

    public function userGetUserByStatus($status)
    {
        $this->db->select('count(user_id) as status');
        $this->db->from('user');
        $this->db->where(array('user_active' => $status));
        return $this->db->get()->row();
    }

    public function get_jeweler_of_limit($limit)
    {
        $this->db->select('jeweler_id,jeweler_name,jeweler_logo,createddate');
        $this->db->from('jeweler');
        $this->db->where('jeweler_active !=', 'Delete');
        $this->db->limit($limit);
        $this->db->order_by('jeweler_id', 'DESC');
        $user = $this->db->get();
        return $user->result_array();
    }

    function get_jeweler_minimum_date()
    {
        $this->db->select('min(createddate) as mindate');
        $query = $this->db->get('jeweler');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

       // get monthly data
    function get_click($type)
    {
        if ($type == "week")
        {
            $today = date('Y-m-d');
            $week = date("W", strtotime($today));
            $year = date("Y", strtotime($today));
            $weekdates = $this->getWeek($week, $year);
            $startdate = $weekdates['start'];
            $enddate = $weekdates['end'];
            $this->db->distinct();
            $this->db->select('count(url_click.agency_id) as agency, agency.agency_name, agency.agency_id, Date(url_click.createddate) as createddate');
            $this->db->join('agency','agency.agency_id = url_click.agency_id');
            $this->db->where('Date(url_click.createddate) >=', $startdate);
            $this->db->where('Date(url_click.createddate) <=', $enddate);
            // $this->db->where('agency_id', $agencyid);
           
            // $this->db->group_by('Date(createddate)');
            $this->db->group_by('url_click.agency_id');
        }
        elseif ($type == "month")
        {
            $startdate = date('Y-m-01', strtotime('this month'));
            $enddate = date('Y-m-t', strtotime('this month'));
            $this->db->select('count(url_click.agency_id) as agency, agency.agency_name, agency.agency_id, Date(url_click.createddate) as createddate');
            $this->db->join('agency','agency.agency_id = url_click.agency_id');
            $this->db->where('Date(url_click.createddate) >=', $startdate);
            $this->db->where('Date(url_click.createddate) <=', $enddate);
            // $this->db->where('agency_id', $agencyid);
           
            // $this->db->group_by('Date(createddate)');
            $this->db->group_by('url_click.agency_id');
        }
        elseif ($type == "year")
        {
            $startdate = date('Y-01-01', strtotime('this month'));
            $enddate = date('Y-12-31', strtotime('this month'));
            $this->db->select('count(url_click.agency_id) as agency, agency.agency_name, agency.agency_id, Date(url_click.createddate) as createddate');
            $this->db->join('agency','agency.agency_id = url_click.agency_id');
            $this->db->where('Date(url_click.createddate) >=', $startdate);
            $this->db->where('Date(url_click.createddate) <=', $enddate);
            // $this->db->where('agency_id', $agencyid);
          
            // $this->db->group_by('MONTH(createddate)');
            $this->db->group_by('url_click.agency_id');
        }
//        echo $startdate.'/'.$enddate;die;
        //Executing Query
        $query = $this->db->get('url_click');
         // echo $this->db->last_query(); 
        return $query->result_array();
    }
    
    
    
    
    function get_call_history_data($type,$call_history)
    {
        if ($type == "week")
        {
            $today = date('Y-m-d');
            $week = date("W", strtotime($today));
            $year = date("Y", strtotime($today));
            $weekdates = $this->getWeek($week, $year);
            $startdate = $weekdates['start'];
            $enddate = $weekdates['end'];
            $this->db->distinct();
            $this->db->select('count(call_sms_history.agency_id) as agency, agency.agency_name,agency.agency_id,Date(call_sms_history.createddate) as createddate');
            $this->db->join('agency','agency.agency_id = call_sms_history.agency_id');
            $this->db->where('Date(call_sms_history.createddate) >=', $startdate);
            $this->db->where('Date(call_sms_history.createddate) <=', $enddate);
            $this->db->where('call_sms_history.history_type', $call_history);
            // $this->db->group_by('Date(createddate)');
            $this->db->group_by('call_sms_history.agency_id');
        }
        elseif ($type == "month")
        {
            $startdate = date('Y-m-01', strtotime('this month'));
            $enddate = date('Y-m-t', strtotime('this month'));
            $this->db->select('count(call_sms_history.agency_id) as agency, agency.agency_name,agency.agency_id,Date(call_sms_history.createddate) as createddate');
            $this->db->join('agency','agency.agency_id = call_sms_history.agency_id');
            $this->db->where('Date(call_sms_history.createddate) >=', $startdate);
            $this->db->where('Date(call_sms_history.createddate) <=', $enddate);
            $this->db->where('call_sms_history.history_type', $call_history);
            // $this->db->group_by('Date(createddate)');
            $this->db->group_by('call_sms_history.agency_id');
        }
        elseif ($type == "year")
        {
            $startdate = date('Y-01-01', strtotime('this month'));
            $enddate = date('Y-12-31', strtotime('this month'));
            $this->db->select('count(call_sms_history.agency_id) as agency, agency.agency_name,agency.agency_id,Date(call_sms_history.createddate) as createddate');
            $this->db->join('agency','agency.agency_id = call_sms_history.agency_id');
            $this->db->where('Date(call_sms_history.createddate) >=', $startdate);
            $this->db->where('Date(call_sms_history.createddate) <=', $enddate);
            $this->db->where('call_sms_history.history_type', $call_history);
            // $this->db->group_by('MONTH(createddate)');
            $this->db->group_by('call_sms_history.agency_id');
        }
//        echo $startdate.'/'.$enddate;die;
        //Executing Query
        $query = $this->db->get('call_sms_history');
        // echo $this->db->last_query(); die;
        return $query->result_array();
    }
    
    
    
   

    function getWeek($week, $year)
    {
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
        return $result;
    }

   

}

?>
