<?php

Class Numbers extends CI_Model {

    public function get_number_data() {
        $this->db->where('number_active !=', 'Delete');
        $this->db->order_by('number_id', 'DESC');
        $result = $this->db->get('number');
        return $result->result_array();
    }

    public function get_numbers_by_id($number_id) {
        $this->db->from('number');
        $this->db->where('number.number_id', $number_id);
        $this->db->order_by('number_id', 'DESC');
        $result = $this->db->get();
        return $result->result_array();
    }

}
