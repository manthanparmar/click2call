<?php

Class admintemplatenumbers extends CI_Model {

    // New function 19th april 2019

    public function get_template_title($actid)
    {
        $this->db->select('*');
        $this->db->from('agency_created_template');
        $this->db->where('actid',$actid);
        // $this->db->where('template_id',$templateid);
        $query = $this->db->get(); 
        return $query->result_array();
    }

    // New function 19th april 2019

    function get_templatetitle_by_angecy_template_id($actid)
    {
        //Executing Query
        $this->db->from('agency_created_template');
        // $this->db->where('template_id', $template_id);
        // $this->db->where('agency_id', $agencyid);
        $this->db->where('actid', $actid);
        $query = $this->db->get();
        $res =  $query->result_array();  
        if(count($res)>0)
        {
           return $res[0]['title'];
        }
        else
        {
            return "";
        }
    }

    // New function 19th april 2019

    function get_call_number_by_angecy_template_id($agencyid,$template_id,$actid)
    {
        //Executing Query
        $this->db->select('tn.*,GROUP_CONCAT(DISTINCT nb.number) as numbers');
        $this->db->from('template_number tn');
        $this->db->join('number nb', 'tn.number_id=nb.number_id', 'left');
        $this->db->where('tn.agency_id', $agencyid);
        $this->db->where('tn.act_id', $actid);
        $this->db->where('tn.template_id', $template_id);
        $this->db->where('tn.type', 'CALL');
        $query = $this->db->get();
        $res =  $query->result_array();  
        if(count($res)>0)
        {
            return $res[0]['numbers'];
        }
        else
        {
            return "";
        }
    }


    // New function 19th april 2019

    function get_sms_number_by_angecy_template_id($agencyid,$template_id,$actid)
    {
        //Executing Query
        $this->db->select('tn.*,GROUP_CONCAT(DISTINCT nb.number) as numbers');
        $this->db->from('template_number tn');
        $this->db->join('number nb', 'tn.number_id=nb.number_id', 'left');
        $this->db->where('tn.agency_id', $agencyid);
        $this->db->where('tn.act_id', $actid);
        $this->db->where('tn.template_id', $template_id);
        $this->db->where('tn.type', 'SMS');
        $query = $this->db->get();
        $res =  $query->result_array();  
        if(count($res)>0)
        {
            return $res[0]['numbers'];
        }
        else
        {
            return "";
        }
    }

    // New function 19th april 2019
    public function delete_code_from_agency_created_template($actid)
    {
        // $this->db->where('agency_id', $agencyid);
        // $this->db->where('template_id', $templateid);
        $this->db->where('actid', $actid);
        if($this->db->delete('agency_created_template'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    // New function 19th april 2019
    function get_code_by_angecy_template_id_new($agencyid,$actid)
    {
        //Executing Query
        $this->db->from('agency_created_template');
        // $this->db->where('template_id', $template_id);
        $this->db->where('actid', $actid);
        $this->db->where('agency_id', $agencyid);
        $query = $this->db->get();
        $res =  $query->result_array();  
        if(count($res)>0)
        {
           return $res[0]['code'];
        }
        else
        {
            return "";
        }
    }


    public function get_agency_data($agencyarray)
    {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->from('agency');
        $this->db->where('agency_active !=', 'Delete');
        $this->db->where_not_in('agency_id', $agencyarray);
        $query = $this->db->get(); 
        return $query->result_array();
    }
    
    public function get_all_agency()
    {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->from('agency');
        $this->db->where('agency_active !=', 'Delete');
        $query = $this->db->get(); 
        return $query->result_array();
    }
    
    public function getalltemplate()
    {
        $query = $this->db->query("SELECT count(*) FROM `template_number` GROUP BY agency_id");
        return $query->result_array();
    }
    
    public function get_agency_not_in()
    {
       return $this->db->query("SELECT *,GROUP_CONCAT(DISTINCT agency_id) as agencyids FROM `template_number` WHERE template_number_active != 'Delete'")->result_array();
    }

       //Date 26th Feb 2019

    public function delete_dt_from_templatenumber($actid)
    {
        /*$this->db->where('agency_id', $agencyid);
        $this->db->where('template_id', $templateid);*/
        $this->db->where('act_id', $actid);
        if($this->db->delete('template_number'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function delete_data_from_templatenumber($actid,$agencyid)
    {
        // $this->db->where('agency_id', $agencyid);
        $this->db->where('act_id', $actid);
        $this->db->where('agency_id', $agencyid);
        $this->db->where('type', 'CALL');
        if($this->db->delete('template_number'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
 
 
      // New function Date 28th March 2019 

    public function delete_smsdata_from_templatenumber($actid,$agencyid)
    {
        $this->db->where('agency_id', $agencyid);
        // $this->db->where('template_id', $templateid);
        $this->db->where('act_id', $actid);
        $this->db->where('type', 'SMS');
        if($this->db->delete('template_number'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //Date 22nd Feb 2019
     public function get_templatedata_for_admin($agencyid)
    {
       
        $this->db->select('tn.template_id');
        $this->db->from('template_number tn');
        $this->db->where('tn.agency_id', $agencyid);
        $this->db->group_by('tn.template_id');
        $query = $this->db->get();
        $res = $query->result_array();
        

        
        $res = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($res)), 0);
        
        // print_r($res); die;
        $this->db->distinct();
        $this->db->select('ag.*,t.template_name');
        $this->db->from('agency_template ag');
        $this->db->join('template t', 't.template_id=ag.template_id', 'left');
        $this->db->where('ag.agency_id', $agencyid);
        if(count($res) > 0)
        {
             //$this->db->where_not_in('ag.template_id', $res);
        }
       
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
        //return $this->db->query("SELECT template_number.*,GROUP_CONCAT(DISTINCT template.template_name) as template_name FROM `template_number` JOIN template ON template_number.template_id = template.template_id WHERE template_number.agency_id = $agencyid")->result_array();    
    }
    
    //Date 22nd Feb 2019
    public function get_numberdata_for_admin($agencyid)
    {
        $this->db->distinct();
        $this->db->select('an.*,n.number');
        $this->db->from('agency_number an');
        $this->db->join('number n', 'n.number_id=an.number_id', 'left');
        $this->db->where('an.agency_id', $agencyid);
        $this->db->where('n.calltype', 'CALL');
        // $this->db->where("(n.calltype='CALL' OR n.calltype='BOTH')");
        $query = $this->db->get();
        
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }
    
    
    //Date 28th March 2019
   //New function for get sms number only
    public function get_smsnumberdata_for_admin($agencyid)
    {
        $this->db->distinct();
        $this->db->select('an.*,n.number');
        $this->db->from('agency_number an');
        $this->db->join('number n', 'n.number_id=an.number_id', 'left');
        $this->db->where('an.agency_id', $agencyid);
        $this->db->where('n.calltype', 'SMS');
        // $this->db->where("(n.calltype='SMS' OR n.calltype='BOTH')");
        $query = $this->db->get();
        
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }
    
    
    //Date 22nd Feb 2019
    function get_template_number_data_for_admin()
    {
        //Executing Query
        $this->db->select('tn.*,tp.template_name as templates');
        $this->db->from('template_number  tn');
        // $this->db->join('number nb', 'suc.number_id=nb.number_id', 'left');
        $this->db->join('shorten_url_code suc', 'tn.template_id=suc.template_id');
        $this->db->join('template tp', 'tn.template_id=tp.template_id', 'left');
        // $this->db->group_by('suc.agency_id');
        $this->db->group_by('tn.template_id');    
        $query = $this->db->get();
        return $query->result_array();  
    }
    
    //Date 22nd Feb 2019
    function get_code_by_angecy_template_id($agencyid,$template_id)
    {
        //Executing Query
        $this->db->from('shorten_url_code');
        $this->db->where('template_id', $template_id);
        $this->db->where('agency_id', $agencyid);
        $query = $this->db->get();
        $res =  $query->result_array();  
        if(count($res)>0)
        {
           return $res[0]['code'];
        }
        else
        {
            return "";
        }
    }


    //Date 22nd Feb 2019
    function get_number_by_angecy_template_id($agencyid,$template_id)
    {
        //Executing Query
        $this->db->select('tn.*,GROUP_CONCAT(DISTINCT nb.number) as numbers');
        $this->db->from('template_number tn');
        $this->db->join('number nb', 'tn.number_id=nb.number_id', 'left');
        $this->db->where('tn.agency_id', $agencyid);
        $this->db->where('tn.template_id', $template_id);
        $query = $this->db->get();
        $res =  $query->result_array();  
        if(count($res)>0)
        {
            return $res[0]['numbers'];
        }
        else
        {
            return "";
        }
    }
    
    
    
    public function get_agency_data_for_admin()
    {
        $this->db->distinct();
        $this->db->select('ag.*');
        $this->db->from('agency ag');
        $this->db->where('ag.agency_active !=','Delete');
        $query = $this->db->get();
        $res =  $query->result_array();  
        if(count($res)>0)
        {
            return $res;
        }
        else
        {
            return "";
        }
    }
    
    
    public function get_template_number_data() {
        $this->db->where('template_number_active !=', 'Delete');
        $this->db->order_by('template_number_id', 'DESC');
        $result = $this->db->get('template_number');
        return $result->result_array();
    }

    public function get_template_numbers_by_id($template_number_id) {
        $this->db->from('template_number');
        $this->db->where('template_number.template_number_id', $template_number_id);
        $this->db->order_by('template_number_id', 'DESC');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_templatenumber_dt($agencyid)
    {
        $this->db->select('tn.*,GROUP_CONCAT(DISTINCT t.template_name) as template_name');
        $this->db->from('template_number tn');
        $this->db->join('template t', 't.template_id=tn.template_id', 'left');
        $this->db->where('tn.agency_id', $agencyid);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
        //return $this->db->query("SELECT template_number.*,GROUP_CONCAT(DISTINCT template.template_name) as template_name FROM `template_number` JOIN template ON template_number.template_id = template.template_id WHERE template_number.agency_id = $agencyid")->result_array();    
    }
    
    public function get_template_data_with_join($agency_id)
    {

        $this->db->select('at.*,t.template_name');
        $this->db->from('agency_template at');
        $this->db->join('template t', 't.template_id=at.template_id', 'left');
        $this->db->where('at.agencytemplate_active !=', 'Delete');
        $this->db->where('at.agency_id',$agency_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
        //return $this->db->query("SELECT agency_template.*,template.template_name FROM `agency_template` JOIN template on agency_template.template_id = template.template_id WHERE agencytemplate_active != 'Delete' AND agency_template.agency_id = $agency_id")->result_array();
    }
    
    public function get_number_data_with_join($agencyid)
    {
        $this->db->distinct();
        $this->db->select('an.*,nb.number');
        $this->db->from('agency_number an');
        $this->db->join('number nb', 'nb.number_id=an.number_id', 'left');
        $this->db->where('an.agencynumber_active !=', 'Delete');
        $this->db->where('an.agency_id',$agencyid);
        $this->db->where('nb.calltype','CALL');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
        //return $this->db->query("SELECT agency_number.*,number.number FROM `agency_number` JOIN number on agency_number.number_id = number.number_id WHERE agencynumber_active != 'Delete' AND agency_number.agency_id = $agency_id")->result_array();
    }
    
    
    
    public function get_smsnumber_data_with_join($agencyid)
    {
        $this->db->distinct();
        $this->db->select('an.*,nb.number');
        $this->db->from('agency_number an');
        $this->db->join('number nb', 'nb.number_id=an.number_id', 'left');
        $this->db->where('an.agencynumber_active !=', 'Delete');
        $this->db->where('an.agency_id',$agencyid);
        $this->db->where('nb.calltype','SMS');
        // $this->db->where("(nb.calltype='SMS' OR nb.calltype='BOTH')");
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
        //return $this->db->query("SELECT agency_number.*,number.number FROM `agency_number` JOIN number on agency_number.number_id = number.number_id WHERE agencynumber_active != 'Delete' AND agency_number.agency_id = $agency_id")->result_array();
    }
    
    // For index method
 
    
    function get_template_with_join()
    {
        //Executing Query
       /* $this->db->distinct();
        $this->db->select('tn.*,tp.template_name as templates,ag.agency_name,GROUP_CONCAT(DISTINCT nb.number) as numbers');
        $this->db->from('template_number  tn');
        $this->db->join('agency ag', 'tn.agency_id=ag.agency_id');
        $this->db->join('number nb', 'tn.number_id=nb.number_id');
        $this->db->join('shorten_url_code suc', 'tn.template_id=suc.template_id');
        $this->db->join('template tp', 'tn.template_id=tp.template_id', 'left');
        // $this->db->group_by('suc.agency_id');
        $this->db->order_by('tn.template_number_id','DESC');    
        $query = $this->db->get();*/
        
        //$query = $this->db->query("SELECT DISTINCT `tn`.*, GROUP_CONCAT( DISTINCT nb.number) as numbers, `tp`.`template_name` as templates, `ag`.`agency_name` FROM (`template_number` tn) JOIN `agency` ag ON `tn`.`agency_id`=`ag`.`agency_id` JOIN `shorten_url_code` suc ON `tn`.`template_id`=`suc`.`template_id` LEFT JOIN `template` tp ON `tn`.`template_id`=`tp`.`template_id` LEFT JOIN `number` nb ON `tn`.`number_id` = `nb`.`number_id` GROUP BY ag.agency_name ORDER BY `tn`.`template_number_id` DESC");
        $query = $this->db->query("
            SELECT DISTINCT `tn`.*, `act`.actid,
            GROUP_CONCAT( DISTINCT nb.number) as numbers, 
            `tp`.`template_name` as templates, 
            `ag`.`agency_name` FROM (`template_number` tn) 
            JOIN `agency` ag ON `tn`.`agency_id`=`ag`.`agency_id` 
            JOIN `agency_created_template` act ON `tn`.`template_id`=`act`.`template_id` 
            LEFT JOIN `template` tp ON `tn`.`template_id`=`tp`.`template_id` 
            LEFT JOIN `number` nb ON `tn`.`number_id` = `nb`.`number_id` 
            GROUP BY tn.act_id,templates
            ORDER BY `tn`.`template_number_id` DESC");
         // echo $this->db->last_query(); die;
        return $query->result_array();  
    }
    
    
    // For Add method
    
    public function get_all_template_ids()
    {
        $this->db->select('tn.*,GROUP_CONCAT(DISTINCT template_id) as templateids,GROUP_CONCAT(DISTINCT number_id) as numbids');
        $this->db->from('template_number tn');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
        //return $this->db->query("SELECT *, GROUP_CONCAT(DISTINCT template_id) as templateids, GROUP_CONCAT(DISTINCT number_id) as numbids FROM `template_number`")->result_array();
    }
    
    public function get_templates_not_in($temparray)
    {
        $this->db->select('*');
        $this->db->from('template');
        $this->db->where_not_in('template_id', $temparray);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
         //return $this->db->query("SELECT * FROM `template` WHERE template_id not in ($temparray)")->result_array();
    }
    
    public function get_templates($agency_id)
    {
         return $this->db->query("SELECT * FROM `template` join `agency_template`  on agency_template.template_id =  template.template_id  WHERE agency_id = $agency_id ")->result_array();
    }
    
    //For edit method
    public function gettemplate_from_tempid($templateid)
    {
        $this->db->query("SELECT *, GROUP_CONCAT(DISTINCT number_id) as numids FROM `template_number` WHERE template_id = $templateid")->result_array();
    }
    
    public function numberids_from_templateid_and_agency_id($actid,$agencyid,$templateid)
    {
        /*return $this->db->query("SELECT *,GROUP_CONCAT(number_id) as numids FROM `template_number` WHERE template_id = $templateid AND agency_id = $agencyid")->result_array(); */   
        return $this->db->query("SELECT *,GROUP_CONCAT(number_id) as numids FROM `template_number` WHERE act_id = $actid AND agency_id = $agencyid AND template_id = $templateid")->result_array(); 
    }
    
    public function templateids_from_templateid_and_agency_id($templateid,$agencyid)
    {
        return $this->db->query("SELECT *,GROUP_CONCAT(DISTINCT template_id) as templateids FROM `template_number` WHERE template_id = $templateid AND agency_id = $agencyid")->result_array();    
    }
    
    //New 21-feb 10:48
    public function get_templates_from_agency_id($actid,$agencyid,$templateid)
    {
       $this->db->select('tn.template_id');
        $this->db->from('template_number tn');
        $this->db->where('tn.agency_id', $agencyid);
        $this->db->where('tn.template_id', $templateid);
        // $this->db->where('tn.template_id !=', $templateid);
        $this->db->where('tn.act_id', $actid);
        $this->db->group_by('tn.template_id');
        $query = $this->db->get();
        $res = $query->result_array();
        
        
        $res = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($res)), 0);
        
        // print_r($res); die;
        $this->db->distinct();
        $this->db->select('ag.*,t.template_name');
        $this->db->from('agency_template ag');
        $this->db->join('template t', 't.template_id=ag.template_id', 'left');
        $this->db->where('ag.agency_id', $agencyid);
        if(count($res) > 0)
        {
             //$this->db->where_not_in('ag.template_id', $res);
        }
       
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
        
        
        
        //  print_r($res); die;
        /*return $this->db->query("SELECT template_number.*,template.template_name FROM `template_number` JOIN template on template_number.template_id = template.template_id WHERE template_number.agency_id = '$agencyid' and
            template.template_id NOT IN $res  GROUP BY template_number.template_id")->result_array();*/
    }
    
    
    // Below method is for mobile view template get by template id
    public function get_template_view_for_mobile($templateid)
    {
        return $this->db->query("SELECT * FROM `template` WHERE template_id = $templateid")->result_array();
    }
    
    public function get_templateid_from_temp_and_agency($actid,$agencyid)
    {
        /*return $this->db->query("SELECT DISTINCT template_id FROM `template_number` WHERE template_number.template_id = '$templateid' AND agency_id = '$agencyid'")->result_array();*/
        return $this->db->query("SELECT DISTINCT template_id FROM `template_number` WHERE act_id = $actid AND agency_id = $agencyid")->result_array();
    }
    
}
