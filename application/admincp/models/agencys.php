<?php

Class Agencys extends CI_Model {

    public function get_agency_data() {
        $this->db->where('agency_active !=', 'Delete');
        $this->db->order_by('agency_id', 'DESC');
        $result = $this->db->get('agency');
        return $result->result_array();
    }

    public function get_agency_by_id($agency_id) {
        $this->db->from('agency');
        $this->db->where('agency.agency_id', $agency_id);
        $this->db->order_by('agency_id', 'DESC');
        $result = $this->db->get();
        return $result->result_array();
    }
    
 

}
