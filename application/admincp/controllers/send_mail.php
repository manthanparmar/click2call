<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Send_mail extends CI_Controller {

    public $paging;
    public $data;

    public function __construct()
    {
        parent::__construct();
        
    }

    //load advertise listing view
    public function index()
    {
        $to = $_REQUEST['to'];
    
        $from = $_REQUEST['from'];
        
        $subject = $_REQUEST['subject'];
        
        $from_name = $_REQUEST['from_name'];
        
        $content = $_REQUEST['content'];
        
         $this->config->load('email', TRUE);
        $this->cnfemail = $this->config->item('email');

        //Loading E-mail Class
        $this->load->library('email');
        $this->email->initialize($this->cnfemail);
        
         $this->email->from($from, $from_name);
        $this->email->to($to); 
        
       $this->email->subject($subject);
        
        $this->email->message($content);
        
        $this->email->send();
        //return true;
         echo $this->email->print_debugger();
    }
}

?>
