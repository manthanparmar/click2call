<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class agencynumber extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/agencynumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $this->load->model('agencynumbers');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'agencynumber';
        $this->data['section_title'] = 'agencynumber';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->library('upload');
        $this->load->model('common');
    }

    public function index()
    {
        $this->data['agencynumbers'] = $this->agencynumbers->get_agency_number_with_join();

        
        $this->data['total'] = count($this->data['agencynumbers']);
        $this->load->view('agencynumber/index', $this->data);
    }

    public function add()
    {
        $this->data['agencynumber_data'] = $this->common->select_database_id($tablename = 'agency_number', $columnname = 'agencynumber_active', $columnid = 'Enable', $data = '*');

        $this->data['agnum'] = $this->agencynumbers->get_agency_number_only();

        $a = array_column($this->data['agnum'], 'numberids');
        if($a[0] == null)
        {
            $a = array(0);
        }
        
        // This below $b is used to implode agency_id from agency number table
        $b = array_column($this->data['agnum'], 'agid');
        if($b[0] == null)
        {
            $b = array(0);
        }
        
        $this->data['number_data'] = $this->agencynumbers->get_number($a[0]);
        $this->data['agency_data'] = $this->agencynumbers->get_agency($b[0]);
        
        $this->load->view('agencynumber/add', $this->data);
    }

    function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version agencynumber 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function view($agencynumberid = NULL)
    {
        if ($agencynumberid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencynumber', 'refresh');
        }
        else
        {
            $agencynumberid = base64_decode($agencynumberid);
            $agencynumbers = $this->common->select_database_id('agencynumber', 'agencynumber_id', (int) $agencynumberid, '*');
            $agencynumbers_contact = $this->common->select_database_id('agencynumber_contact', 'agencynumber_id', (int) $agencynumberid, '*');

            if (count($agencynumbers) > 0)
            {
                $this->data['subscription_data'] = $this->agencynumbers->get_subscription_by_id($agencynumberid);
                //$this->data['subscription_data'] = array();
                //echo "<pre>";print_r($this->data['subscription_data']);die();
                $this->data['agencynumbers'] = $agencynumbers;
                $this->data['agencynumbers_contact'] = $agencynumbers_contact;
            }
            else
            {
                $this->data['agencynumbers'] = array();
            }

            //Loading View File
            $this->load->view('agencynumber/view', $this->data);
        }
    }

    public function addagencynumber()
    {
        $session_data = $this->session->userdata['devclick_admin'];
        $adminid = $session_data['adminid'];

            $agencyid =  $this->input->post('agency_id');
            $agency_numbers_list = $this->input->post('agency_numbers_list');

            // add agency number list details in agency call table
            if (!empty($agency_numbers_list))
            {
                for ($k = 0; $k < count($agency_numbers_list); $k++)
                {
                    $agencynumber_data = array(
                        'agency_id' => $agencyid,
                        'number_id' => $agency_numbers_list[$k],
                        'agencynumber_active' => 'Enable',
                        'createddate' => date('Y-m-d H:i:s')
                    );
                 // echo "<pre>"; print_r($agencynumber_data);
                    $this->common->insert_data($agencynumber_data, 'agency_number');            
                }
            $this->session->set_flashdata('success', 'Agency number inserted successfully.');
                redirect('agencynumber', 'refresh');
                
            }
            else
            {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                        redirect('agencynumber', 'refresh');
            }
    }

    public function delete($agencynumberid = NULL)
    {
        if ($agencynumberid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencynumber', 'refresh');
        }
        else
        {
            $agencyid = $this->input->post('agency_id');
            // print_r($agencyid); die;
            if ($this->common->update_data(array('agencynumber_active' => 'Delete'), 'agency_number', 'agency_id', (int) $agencyid))
            {
               /*This below query is do free number while delete from agancy*/
                $this->db->set('number_id', '0');
                $this->db->where('agencynumber_active', 'Delete');
                $this->db->update('agency_number');
                
                $this->session->set_flashdata('success', 'Agency number deleted successfully.');
                redirect('agencynumber', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('agencynumber', 'refresh');
            }
        }
    }

    public function edit($agencyid = NULL)
    {
        if ($agencyid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencynumber', 'refresh');
        }
        else
        {
            $agencyid = base64_decode($agencyid);
            $this->data['agencynumbers'] = $this->common->select_database_id($tablename = 'agency_number', $columnname = 'agency_id', $columnid = $agencyid, $data = '*');
            
            $this->data['agencynum'] = $this->agencynumbers->getnumberids_from_agnum($agencyid);
            // Agencynumber's exist number id    print_r($this->data['agencynum']); die;
            
            $this->data['agencynum_not_exist'] = $this->agencynumbers->getnumberids_from_agnum_not_exist($agencyid);
           // Agencynumber's not exist number id   echo "<pre>"; print_r($this->data['agencynum_not_exist']); die;
            
            $a = array_column($this->data['agencynum_not_exist'], 'nids');
            if($a[0] == null)
            {
                $a = array(0);
            }
        
            $b = array_column($this->data['agencynum_not_exist'], 'agnids');
            if($a[0] == null)
            {
                $b = array(0);
            }
            
            $this->data['numbertable_data'] = $this->agencynumbers->get_number($a[0]);
            $this->data['agencytable_data'] = $this->agencynumbers->get_agency($b[0]);
            
            // echo "<pre>"; print_r($this->data['numbertable_data']); die;
      
            $this->load->view('agencynumber/edit', $this->data);
        }
    }

//Updating the record
    public function update()
    {
        if ($this->input->post('ag_id'))
        {
           
            $agency_id = base64_decode($this->input->post('ag_id'));
           
            
            $agency_numbers_list = $this->input->post('agency_numbers_list');
            
            if (!empty($agency_numbers_list))
            {
                $this->common->delete_data($tablename='agency_number', $columnname='agency_id', $agency_id);
                for ($k = 0; $k < count($agency_numbers_list); $k++)
                {
                    $agencynumber_data = array(
                        'agency_id' => $agency_id,
                        'number_id'=>$agency_numbers_list[$k],
                        'agencynumber_active' => 'Enable',
                        'createddate' => date('Y-m-d H:i:s')
                    );
                   
                    $this->common->insert_data($agencynumber_data, $tablename='agency_number');
                
                }
                
                
               
                $this->session->set_flashdata('success', 'Agency number updated successfully.');
                redirect('agencynumber', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('agencynumber', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencynumber', 'refresh');
        }
    }

    public function viewmodal($mode, $type_agencynumber_active, $id,$agencyid)
    {
        $data['type_status'] = $type_agencynumber_active;
        $data['mode'] = $mode;
        $data['id'] = $id;
        $data['agencyid'] = $agencyid;

        echo $this->load->view('agencynumber/modal', $data, true);
    }

    public function changestatus()
    {
        $agencynumberid = $this->input->post('agencynumber_id');
        $agencynumber_active = $this->input->post('agencynumber_active');
        $status_msg = $this->input->post('status_msg');

        // print_r($this->input->post(NULL,TRUE));die();
        if (isset($this->session->userdata['devclick_admin']))
        {
            if (!$agencynumberid)
            {
                $this->session->set_flashdata('message', 'Specified id not found.');
                redirect('agencynumber', 'refresh');
            }
            if ($this->common->update_data(array('agencynumber_active' => $agencynumber_active), 'agency_number', 'agencynumber_id', (int) $agencynumberid))
            {
                $agencynumberdata = $this->common->select_database_id('agency_number', 'agencynumber_id', (int) $agencynumberid, '*');
                /*
                  //$app_name_setting = $this->common->select_database_id('setting','AppSettingID',1);
                  $app_name = $this->common->get_setting_value(1);
                  $app_mail = $this->common->get_setting_value(6);
                  $this->load->library('email');
                  //Loading E-mail config file
                  $this->config->load('email', TRUE);
                  $this->cnfemail = $this->config->item('email');
                  $this->email->initialize($this->cnfemail);
                  $this->email->from($app_mail, $app_name);
                  $this->email->to($agencynumberdata[0]['agencynumber_email']);
                  if($agencynumber_active =="Disable"){
                  $this->email->subject('Rest : agencynumber Disable');
                  }
                  else
                  {
                  $this->email->subject('rest : agencynumber Enable');
                  }
                  $mail_body = "<p>Hello&nbsp; " . $agencynumberdata[0]['agencynumber_name'] . ",</p>

                  <table>
                  <tbody>
                  <tr>
                  <td> Email</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $agencynumberdata[0]['agencynumber_email'] . "</td>
                  </tr>

                  <tr>
                  <td> Message</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $status_msg . ".</td>
                  </tr>

                  </tbody>
                  </table>
                  <p>
                  <span>If you have any question or trouble, please contact an app administrator.</span></p>
                  <p>&nbsp;</p>
                  <p>Regards,<br/>
                  " . $app_name . " Team.</p>";

                  //echo $mail_body;die();
                  $this->email->message($mail_body);

                  $this->email->send(); */

                $this->session->set_flashdata('success', 'Agency number status changed to ' . $agencynumber_active . '.');
                redirect('agencynumber', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'There is error in updating agencynumber status.Try later!');
                redirect('agencynumber', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Something went wrong.Please try later!');
            redirect('dashboard', 'refresh');
        }
    }

    public function email_exist($agencynumberid = NULL)
    {
//        print_r($this->input->post(NULL,TRUE));die();

        $agencynumbermail = $this->input->post('agencynumberemail');

        if ($agencynumbermail != '')
        {

            if ($agencynumberid == NULL)
            {
                if ($this->common->check_unique_avalibility('agencynumber', 'agencynumber_email', $agencynumbermail, 'agencynumber_active', 'Delete'))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
            else
            {
                if ($this->common->check_unique_avalibility('agencynumber', 'agencynumber_email', $agencynumbermail, 'agencynumber_id', (int) base64_decode($agencynumberid), array('agencynumber_active !=' => 'Delete')))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
        }
        else
        {
            $response = array('valid' => false, 'message' => 'Enter valid email.');
            echo json_encode($response);
            die();
        }

    }

    public function multipleEvent()
    {
        if ($this->input->post('event') == 'Enable')
        {
            $enableId = $this->input->post('check');
            if ($enableId != '' && $enableId != 0)
            {
                for ($i = 0; $i < count($enableId); $i++)
                {
                    $allId[] = $enableId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'agencynumber_active', $value = "Enable", 'agencynumber_id', 'agencynumber'))
                {
                    $this->session->set_flashdata('success', 'Agency number status changed to Enable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('agencynumber', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating agencynumber status. Try later');
                    redirect('agencynumber', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Enable. Try later!');
                redirect('agencynumber', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Disable')
        {
//check permission
// disable multiple item
            $disableId = $this->input->post('check');
            if ($disableId != '' && $disableId != 0)
            {
                for ($i = 0; $i < count($disableId); $i++)
                {
                    $allId[] = $disableId[$i];
                }

                if ($this->common->multipleEvent($allId, $status_columnname = 'agencynumber_active', $value = "Disable", 'agencynumber_id', 'agencynumber'))
                {
                    $this->session->set_flashdata('success', 'Agency number status changed to Disable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('agencynumber', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating agencynumber status. Try later');
                    redirect('agencynumber', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Disable. Try later!');
                redirect('agencynumber', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Delete')
        {

            $deleteId = $this->input->post('check');
            if ($deleteId != '' && $deleteId != 0)
            {
                for ($i = 0; $i < count($deleteId); $i++)
                {
                    $allId[] = $deleteId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'agencynumber_active', $value = "Delete", 'agencynumber_id', 'agencynumber'))
                {
                    $this->session->set_flashdata('success', 'Agency number deleted successfully.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('agencynumber', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in deleting agencynumber. Try later');
                    redirect('agencynumber', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Delete. Try later!');
                redirect('agencynumber', 'refresh');
            }
        }
    }


}

/* End of file agencynumber.php */
/* Location: ./application/controllers/agencynumber.php */
