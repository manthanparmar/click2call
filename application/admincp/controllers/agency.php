<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class agency extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/agency_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $this->load->model('agencys');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'agency';
        $this->data['section_title'] = 'agency';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->library('upload');
        $this->load->model('common');
    }

    public function index()
    {
        $this->data['agencys'] = $this->agencys->get_agency_data();
        // print_r($this->data['agencys']); die;
        $this->data['total'] = count($this->data['agencys']);
        // print_r($this->data['agencys']); die;
        $this->load->view('agency/index', $this->data);
    }

    public function add()
    {
        $this->data['agency_data'] = $this->common->select_database_id($tablename = 'agency', $columnname = 'agency_active', $columnid = 'Enable', $data = '*');
        // $this->data['uuid'] = strtoupper($this->gen_uuid());
        $this->load->view('agency/add', $this->data);
    }

    function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version agency 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function view($agencyid = NULL)
    {
        if ($agencyid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agency', 'refresh');
        }
        else
        {
            $agencyid = base64_decode($agencyid);
            $agency = $this->common->select_database_id('agency', 'agency_id', (int) $agencyid, '*');
            
            if (count($agency) > 0)
            {
                $this->data['agencys_data'] = $this->agencys->get_agency_by_id($agencyid);
                //$this->data['subscription_data'] = array();
                //echo "<pre>";print_r($this->data['subscription_data']);die();
                $this->data['agency'] = $agency;
                $this->data['agency_contact'] = $agency_contact;
            }
            else
            {
                $this->data['agency'] = array();
            }

            //Loading View File
            $this->load->view('agency/view', $this->data);
        }
    }

    public function addagency()
    {
        $session_data = $this->session->userdata['devclick_admin'];
        $adminid = $session_data['adminid'];

            $app_name = $this->common->get_setting_value(1);
            $data = array(
                'agency_name' => $this->input->post('agencyname'),
                'agency_email' => $this->input->post('agencyemail'),
                'agency_password' => $this->input->post('agencypassword'),
                'api_username' => $this->input->post('api_username'),
                'api_password' => $this->input->post('api_password'),
                'agency_active' => 'Enable',
                'createdby' => $adminid,
                'createddate' => date('Y-m-d h:i:s')
            );
            
            if ($this->common->insert_data($data, 'agency'))
            {
                $agencyid = $this->db->insert_id();
                
             
               /* $payoutcall = $this->input->post('payout_call');
                $payoutsms = $this->input->post('payout_sms');
                $payoutcountry = $this->input->post('payout_country');

             // add agency call details in agency call table
                if (!empty($payoutcall))
                {
                    for ($k = 0; $k < count($payoutcall); $k++)
                    {
                        $payout_data = array(
                            'agency_id' => $agencyid,
                            'agency_call' => $payoutcall[$k],
                            'agency_sms' => $payoutsms[$k],
                            'country' => $payoutcountry[$k],
                            'createddate' => date('Y-m-d H:i:s')
                        );
                        $this->common->insert_data($payout_data, 'agency_call');
                    }
                    
                     
                }*/
                
                
                
                //Below array is insert in agency_setting template
                $agency_setting_data = array(
                    'agency_id' => $agencyid,
                    'agency_email' => $this->input->post('agencyemail'),
                    'agency_name' => $this->input->post('agencyname'),
                    'createddate' => date('Y-m-d h:i:s')
                );
                
                $this->common->insert_data($agency_setting_data,'agency_setting');
                
                   //Send Email to agency 
                $agencyname = $this->input->post('agencyname');
                $subject = $agencyname." "."Login Credential";
                $useremail = $this->input->post('agencyemail');
                $app_mail = $this->input->post('agencyemail');
                $mail_body = "Username is : ".$this->input->post('agencyemail')."<br> Password is :".$this->input->post('agencypassword');
            
                //$this->sendEmail($useremail,$admin_email[0]['adminemail'],$subject, $mail_body);
                $this->sendEmail($app_name, $useremail, $app_mail, $subject,$mail_body);
                
                
                $this->session->set_flashdata('success', 'Agency inserted successfully.');
                redirect('agency', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('agency', 'refresh');
            }
    }

    public function delete($agencyid = NULL)
    {
        if ($agencyid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agency', 'refresh');
        }
        else
        {
            if ($this->common->update_data(array('agency_active' => 'Delete'), 'agency', 'agency_id', (int) $agencyid))
            {
                $this->session->set_flashdata('success', 'Agency deleted successfully.');
                redirect('agency', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('agency', 'refresh');
            }
        }
    }

    public function edit($agencyid = NULL)
    {
        if ($agencyid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agency', 'refresh');
        }
        else
        {
            $agencyid = base64_decode($agencyid);
            $this->data['agencys'] = $this->common->select_database_id('agency', 'agency_id', (int) $agencyid, '*');
            $this->data['agency_payout'] = $this->common->select_database_id('agency_call', 'agency_id', (int) $this->data['agencys'][0]['agency_id'], '*');

            // echo "<pre>"; print_r($this->data['agency_payout']); die;
            $this->load->view('agency/edit', $this->data);
        }
    }

//Updating the record
    public function update()
    {
        if ($this->input->post('agency_id'))
        {
            $agencyid = base64_decode($this->input->post('agency_id'));
           
            $data = array(
                'agency_name' => $this->input->post('agencyname'),
                'agency_email' => $this->input->post('agencyemail'),
                'api_username' => $this->input->post('api_username'),
                'api_password' => $this->input->post('api_password'),
                'agency_active' => 'Enable',
                'modifieddate' => date('Y-m-d h:i:s'),
            );

            if ($this->common->update_data($data, 'agency', 'agency_id', (int) $agencyid))
            {
               /* 
                $payoutcall = $this->input->post('payout_call');
                $payoutsms = $this->input->post('payout_sms');
                $payoutcountry = $this->input->post('payout_country');

             // add agency call details in agency call table
                if (!empty($payoutcall))
                {
                    $this->common->delete_data('agency_call', 'agency_id', $agencyid);
                    for ($k = 0; $k < count($payoutcall); $k++)
                    {
                        $payout_data = array(
                            'agency_id' => $agencyid,
                            'agency_call' => $payoutcall[$k],
                            'agency_sms' => $payoutsms[$k],
                            'country' => $payoutcountry[$k],
                            'createddate' => date('Y-m-d H:i:s')
                        );
                        $this->common->insert_data($payout_data, 'agency_call');
                    }
                }*/

                $this->session->set_flashdata('success', 'Agency updated successfully.');
                redirect('agency', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('agency', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agency', 'refresh');
        }
    }

    public function viewmodal($mode, $type_agency_active, $id)
    {
        $data['type_status'] = $type_agency_active;
        $data['mode'] = $mode;
        $data['id'] = $id;

        echo $this->load->view('agency/modal', $data, true);
    }

    public function changestatus()
    {
        $agencyid = $this->input->post('agency_id');
        $agency_active = $this->input->post('agency_active');
        $status_msg = $this->input->post('status_msg');

        // print_r($this->input->post(NULL,TRUE));die();
        if (isset($this->session->userdata['devclick_admin']))
        {
            if (!$agencyid)
            {
                $this->session->set_flashdata('message', 'Specified id not found.');
                redirect('agency', 'refresh');
            }
            if ($this->common->update_data(array('agency_active' => $agency_active), 'agency', 'agency_id', (int) $agencyid))
            {
                $agencydata = $this->common->select_database_id('agency', 'agency_id', (int) $agencyid, '*');
                /*
                  //$app_name_setting = $this->common->select_database_id('setting','AppSettingID',1);
                  $app_name = $this->common->get_setting_value(1);
                  $app_mail = $this->common->get_setting_value(6);
                  $this->load->library('email');
                  //Loading E-mail config file
                  $this->config->load('email', TRUE);
                  $this->cnfemail = $this->config->item('email');
                  $this->email->initialize($this->cnfemail);
                  $this->email->from($app_mail, $app_name);
                  $this->email->to($agencydata[0]['agency_email']);
                  if($agency_active =="Disable"){
                  $this->email->subject('Rest : agency Disable');
                  }
                  else
                  {
                  $this->email->subject('rest : agency Enable');
                  }
                  $mail_body = "<p>Hello&nbsp; " . $agencydata[0]['agency_name'] . ",</p>

                  <table>
                  <tbody>
                  <tr>
                  <td> Email</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $agencydata[0]['agency_email'] . "</td>
                  </tr>

                  <tr>
                  <td> Message</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $status_msg . ".</td>
                  </tr>

                  </tbody>
                  </table>
                  <p>
                  <span>If you have any question or trouble, please contact an app administrator.</span></p>
                  <p>&nbsp;</p>
                  <p>Regards,<br/>
                  " . $app_name . " Team.</p>";

                  //echo $mail_body;die();
                  $this->email->message($mail_body);

                  $this->email->send(); */

                $this->session->set_flashdata('success', 'Agency status changed to ' . $agency_active . '.');
                redirect('agency', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'There is error in updating agency status.Try later!');
                redirect('agency', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Something went wrong.Please try later!');
            redirect('dashboard', 'refresh');
        }
    }

    public function email_exist($agencyid = NULL)
    {
//        print_r($this->input->post(NULL,TRUE));die();

        $agencymail = $this->input->post('agencyemail');
        
        if ($agencymail != '')
        {
            if ($agencyid == NULL)
            {
                if ($this->common->check_unique_avalibility('agency', 'agency_email', $agencymail, 'agency_active', 'Delete'))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
            else
            {
                if ($this->common->check_unique_avalibility('agency', 'agency_email', $agencymail, 'agency_id', (int) base64_decode($agencyid), array('agency_active !=' => 'Delete')))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
        }
        else
        {
            $response = array('valid' => false, 'message' => 'Enter valid email.');
            echo json_encode($response);
            die();
        }

    }


    function sendEmail($app_name='',$app_email='',$to_email='',$subject='',$mail_body='')
    {
         $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => '465',
            'smtp_user' => "aspltest3@gmail.com", 
            'smtp_pass' => "Aspl@1234", // change it to yours
            'mailtype'  => 'html', 
            'charset'   => 'utf8'
        );
        
        
        $this->config->load('email', TRUE);
        $this->cnfemail = $this->config->item('email');

        //Loading E-mail Class
        $this->load->library('email');
        $this->email->initialize($this->cnfemail);
        
        $this->email->from("aspltest3@gmail.com",$app_name);
        
        $this->email->to($to_email);
        
        $this->email->subject($subject);
        
        
        $this->email->message("<table border='0' cellpadding='0' cellspacing='0'><tr><td></td></tr><tr><td>" . $mail_body . "</td></tr></table>");
        $this->email->send();
        //   $this->email->print_debugger();

      
        return;
    }
    
    function sendEmails($email='', $to_email='', $subject='',$mail_body='')
    {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.zoho.com',
            'smtp_port' => '465',
            'smtp_user' => "manthan.p@ashapurasoftech.com", 
            'smtp_pass' => "manthan@123", // change it to yours
            'mailtype'  => 'html', 
            'charset'   => 'utf8'
        );
        $sender="manthan.p@ashapurasoftech.com";
        $this->load->library('email',$config);

        $this->email->set_newline("\r\n");
        $this->email->set_crlf( "\r\n" );

        $this->email->from($sender);

        $this->email->to($email);
    
        $this->email->subject($subject);
        
        $this->email->message($mail_body);


        if($this->email->send())
        {
              return true;
        }
        else
        {
             return false;
        }
        die;
    }
   
}

/* End of file agency.php */
/* Location: ./application/controllers/agency.php */
