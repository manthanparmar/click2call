<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login page
            redirect('adminlogin', 'refresh');
        }
        include('include.php');
        //Setting Page Title and Comman Variable
        $this->data['title'] = 'Administrator Dashboard';
        $this->data['section_title'] = 'Dashboard';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);
        $this->load->model('common');
        $this->load->model('dashboardmodel');
        $this->load->model('agencynumbers');
        $this->load->model('agencys');

        //Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
        //Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
    }

    function getWeek($week, $year)
    {
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
        return $result;
    }

    public function index()
    {

        //$this->load->view('dashboard',$this->data);
        if (isset($this->session->userdata['devclick_admin']))
        {

            $from_date = date('Y-m-d H:i:s');
            $to_date = date('Y-m-d H:i:s');
            $agencyid = '';
            $calltype = '';    
           
            $this->data['from_date'] = $from_date;
            $this->data['to_date'] = $to_date;
            $this->data['calltype'] = $calltype;
            $this->data['agencyid'] = $agencyid;
            $this->data['filtered_data'] = $this->dashboardmodel->get_history_filter_data($from_date,$to_date,$agencyid,$calltype);


            $this->data['agency_data'] = $this->agencys->get_agency_data();
            $this->data['total_number'] = count($this->common->get_record('number', 'number_active !=', 'Delete'));
            $this->data['total_agency'] = count($this->common->get_record('agency', 'agency_active !=', 'Delete'));
            $this->data['total_agencynumber'] = count($this->common->get_record('agency_number', 'agencynumber_active !=', 'Delete'));
            $this->data['total_agencytemplate'] = count($this->common->get_record('agency_template', 'agencytemplate_active !=', 'Delete'));

            $this->load->view('dashboard', $this->data);
        }
        else
        {
            redirect('adminlogin', 'refresh');
        }
    }


    function get_filtered_data()
    {
        if(isset($_POST['submit']))
        {
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];
            $agencyid = $_POST['agency_id'];
            $calltype = $_POST['calltype'];    
        }
        
        $this->data['total_number'] = count($this->common->get_record('number', 'number_active !=', 'Delete'));
        $this->data['total_agency'] = count($this->common->get_record('agency', 'agency_active !=', 'Delete'));
        $this->data['total_agencynumber'] = count($this->common->get_record('agency_number', 'agencynumber_active !=', 'Delete'));
        $this->data['total_agencytemplate'] = count($this->common->get_record('agency_template', 'agencytemplate_active !=', 'Delete'));

        $this->data['agency_data'] = $this->agencys->get_agency_data();
        $this->data['from_date'] = $from_date;
        $this->data['to_date'] = $to_date;
        $this->data['calltype'] = $calltype;
        $this->data['agencyid'] = $agencyid;
        $this->data['filtered_data'] = $this->dashboardmodel->get_history_filter_data($from_date,$to_date,$agencyid,$calltype);

        $this->load->view('dashboard', $this->data);
    }



   //click data
    function get_click_data()
    {
        $type = $this->input->post('type');
        
        
        if ($type == 'week')
        {
            $user_data = $this->dashboardmodel->get_click('week');
        }
        elseif ($type == 'year')
        {

            $user_data = $this->dashboardmodel->get_click('year');
        }
        elseif ($type == 'month')
        {
            $today = date('Y-m-d');
            $user_data = $this->dashboardmodel->get_click('month');
        }
        $month_array = array();


            $total_agency_data = $this->dashboardmodel->total_agency();

             // echo "<pre>";  print_r($user_data); 


            // $total_day = date('t', strtotime('this month'));
            for ($i = 0; $i < count($total_agency_data); $i++)
            {
                if (!empty($user_data))
                {
                    // 
                    for ($j = 0; $j < count($user_data); $j++)
                    {


                        if (isset($user_data[$j]['agency_id']) && $user_data[$j]['agency_id'] == $total_agency_data[$i]['agency_id'])
                        {
                            $month_array[$i]['agencyname'] = $total_agency_data[$i]['agency_name'];
                            $month_array[$i]['Visitors'] = $user_data[$j]['agency'];

                             break;
                         
                        }
                        else
                        {  
                            $month_array[$i]['agencyname'] = $total_agency_data[$i]['agency_name'];
                            $month_array[$i]['Visitors'] = 0;
                            
                        }

                    }
                    /*echo "hello1"; print_r($month_array);
                    die;*/
                }
                else
                {
                    
                     
                        $month_array[$i]['agencyname'] = $total_agency_data[$i]['agency_name'];
                        $month_array[$i]['Visitors'] = 0;
                    
                }

            }
            
            // echo "<pre>";  print_r($month_array); 
            echo json_encode($month_array);
            die;
    }
    
    
    
    
    
    // CALL SMS GRAPH
    
    function get_callhistory_data()
    {
        $type = $this->input->post('type');
        $call_history = $this->input->post('call_history');
        
        
        if ($type == 'week')
        {
            $user_data = $this->dashboardmodel->get_call_history_data('week',$call_history);
        }
        elseif ($type == 'year')
        {

            $user_data = $this->dashboardmodel->get_call_history_data('year',$call_history);
        }
        elseif ($type == 'month')
        {
            $today = date('Y-m-d');
            $user_data = $this->dashboardmodel->get_call_history_data('month',$call_history);
        }
        $month_array = array();


            $total_agency_data = $this->dashboardmodel->total_agency();

             // echo "<pre>";  print_r($user_data); 


            // $total_day = date('t', strtotime('this month'));
            for ($i = 0; $i < count($total_agency_data); $i++)
            {
                if (!empty($user_data))
                {
                    // 
                    for ($j = 0; $j < count($user_data); $j++)
                    {


                        if (isset($user_data[$j]['agency_id']) && $user_data[$j]['agency_id'] == $total_agency_data[$i]['agency_id'])
                        {
                            $month_array[$i]['agencyname'] = $total_agency_data[$i]['agency_name'];
                            $month_array[$i]['total'] = $user_data[$j]['agency'];

                             break;
                         
                        }
                        else
                        {  
                            $month_array[$i]['agencyname'] = $total_agency_data[$i]['agency_name'];
                            $month_array[$i]['total'] = 0;
                            
                        }

                    }
                    /*echo "hello1"; print_r($month_array);
                    die;*/
                }
                else
                {
                    
                     
                        $month_array[$i]['agencyname'] = $total_agency_data[$i]['agency_name'];
                        $month_array[$i]['total'] = 0;
                    
                }

            }
            
            // echo "<pre>";  print_r($month_array); 
            echo json_encode($month_array);
            die;
    }





    function time_elapsed_string($ptime)
    {

        $etime = strtotime(date('Y-m-d H:i:s')) - strtotime($ptime);

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

    function logout()
    {
        if (isset($this->session->userdata['devclick_admin']))
        {
            $this->session->unset_userdata('devclick_admin');
            $this->session->sess_destroy();
            redirect('adminlogin', 'refresh');
        }
        else
        {
            $this->session->unset_userdata('devclick_admin');
            $this->session->sess_destroy();
            redirect('adminlogin', 'refresh');
        }
    }

    public function jeweler($utype = '')
    {
        //$this->load->view('dashboard',$this->data);
        if (isset($this->session->userdata['devclick_admin']))
        {
            $this->data['user'] = $this->dashboardmodel->get_user_of_limit(8);
            $this->data['total_user'] = count($this->common->get_record('user', 'user_active !=', 'Delete'));
            $this->data['total_restaurant'] = count($this->common->get_record('jeweler', 'jeweler_active !=', 'Delete'));
            $this->data['total_beacon'] = count($this->common->get_record('beacon', 'beacon_active !=', 'Delete'));
            $this->data['total_subscription'] = count($this->common->get_record('subscription', 'sub_active !=', 'Delete'));

            if ($utype == "" || $utype == "weekly")
            {
                $getdate = strtotime(date("Y-m-d"));
                // calculate the number of days since Monday
                $dow = date('w', $getdate);
                $offset = $dow - 1;
                if ($offset < 0)
                {
                    $offset = 6;
                }
                // calculate timestamp for Monday and Sunday
                $monday = $getdate - ($offset * 86400);
                $sunday = $monday + (6 * 86400);

                // print dates for Monday and Sunday in the current week
                $startdate = date("Y-m-d", $monday);
                $enddate = date("Y-m-d", $sunday);

                $this->data['enddate'] = $enddate;
            }
            elseif ($utype == "monthly")
            {
                
            }
            elseif ($utype == "monthyear")
            {
                $this->data['syear'] = $this->input->post('syear');
                $this->data['smonth'] = $this->input->post('smonth');
            }
            $this->data['mindate'] = $this->dashboardmodel->get_jeweler_minimum_date();
            $this->data['type'] = 'jeweler';

            $this->load->view('dashboard', $this->data);
        }
        else
        {
            redirect('adminlogin', 'refresh');
        }
    }

}

/* End of file dashboard.php */
    /* Location: ./application/controllers/dashboard.php */
    
