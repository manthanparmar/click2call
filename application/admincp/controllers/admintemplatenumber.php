<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admintemplatenumber extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/admintemplatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $this->load->model('admintemplatenumbers');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'admintemplatenumber';
        $this->data['section_title'] = 'admintemplatenumber';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->library('upload');
        $this->load->model('common');
         $agencyid = '1';
    }

    public function index()
    {
        $sessionarray = $this->session->userdata('devclick_admin');
        //$this->data['admintemplatenumbers'] = $this->admintemplatenumbers->get_template_number_data();
       
        $this->data['templatenumbers'] = $templatenumbers = $this->admintemplatenumbers->get_template_with_join();
        $get_all_template = $this->admintemplatenumbers->getalltemplate();
       // echo "<pre>";print_r($this->data['templatenumbers']);die;
        
        //print_r(count($get_all_template)); die;
        for($i=0;$i<count($this->data['templatenumbers']);$i++)
        {
            $code = $this->admintemplatenumbers->get_code_by_angecy_template_id($templatenumbers[$i]['agency_id'],$templatenumbers[$i]['template_id']);

            /*$code_new = $this->admintemplatenumbers->get_code_by_angecy_template_id_new($templatenumbers[$i]['agency_id'],$templatenumbers[$i]['template_id']);*/

            $code_new = $this->admintemplatenumbers->get_code_by_angecy_template_id_new($templatenumbers[$i]['agency_id'],$templatenumbers[$i]['act_id']);

            $template_title = $this->admintemplatenumbers->get_templatetitle_by_angecy_template_id($templatenumbers[$i]['act_id']);
            $template_number = $this->admintemplatenumbers->get_number_by_angecy_template_id($templatenumbers[$i]['agency_id'],$templatenumbers[$i]['template_id']);

            $template_call_number = $this->admintemplatenumbers->get_call_number_by_angecy_template_id($templatenumbers[$i]['agency_id'],$templatenumbers[$i]['template_id'],$templatenumbers[$i]['act_id']);

            $template_sms_number = $this->admintemplatenumbers->get_sms_number_by_angecy_template_id($templatenumbers[$i]['agency_id'],$templatenumbers[$i]['template_id'],$templatenumbers[$i]['act_id']);
              
             $this->data['templatenumbers'][$i]['code'] = $code;
             $this->data['templatenumbers'][$i]['code_new'] = $code_new;
             // $this->data['templatenumbers'][$i]['numbers'] = $template_number;
             $this->data['templatenumbers'][$i]['callnumbers'] = $template_call_number;
             $this->data['templatenumbers'][$i]['smsnumbers'] = $template_sms_number;
             $this->data['templatenumbers'][$i]['title'] = $template_title;
             $this->data['templatenumbers'][$i]['actid'] = $templatenumbers[$i]['act_id'];
        } 
     
       // echo "<pre>"; print_r($this->data['templatenumbers']); die;
        $this->load->view('admintemplatenumber/index',$this->data);
    }

    public function add()
    {
        
        $agencyid = '1';
        $this->data['admintemplatenumber_data'] = $this->common->select_database_id($tablename = 'template_number', $columnname = 'template_number_active', $columnid = 'Enable', $data = '*');
        
        
        $this->data['agency_not_exist'] = $this->admintemplatenumbers->get_agency_not_in();        
        
        // print_r($this->data['agency_not_exist']); die;
        
        $a = array_column($this->data['agency_not_exist'], 'agencyids');
      /*  if($a[0] == null)
        {
            $a = array(0);
        }*/
        
        // $a = explode(',',$a[0]);
        $this->data['agency_data'] = $this->admintemplatenumbers->get_all_agency();
      
     /*   $a = array_column($this->data['temp_dt'], 'templateids');
        if($a[0] == null)
        {
            $a = array(0);
        }*/
        
        // print_r($a); die;
        
       /* $used_template = $this->common->select_database_id($tablename = 'template_number', $columnname = 'agency_id', $agencyid , $data = 'template_id');
     
        $used_template_id = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($used_template)), 0);
        $this->data['template_data'] = $this->admintemplatenumbers->get_templates_not_in($a[0]);
        $this->data['template_data'] = $this->common->get_all_assign_templates($agencyid,$used_template_id);
       
        // echo "<pre>";print_r( $this->data['template_data']);die;
        $this->data['number_data'] = $this->admintemplatenumbers->get_number_data_with_join($agencyid);*/
            
        $this->load->view('admintemplatenumber/add', $this->data);
    }

    function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version admintemplatenumber 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function addadmintemplatenumber()
    {
            $templatetitle = $this->input->post('title');
            $template_id=  $this->input->post('template_id');
            $agency_id=  $this->input->post('agency_id');
            $template_number_list = $this->input->post('template_number_list');
            $template_smsnumber_list = $this->input->post('template_smsnumber_list');

            $agency_created_template_data = array(
                'agency_id' => $agency_id,
                'template_id' => $template_id,
                'title' => $templatetitle,
                'code' => $this->generateRandomString('6'),
                'created_date' => date('Y-m-d H:i:s')
            );
                 // echo "<pre>"; print_r($admintemplatenumber_data);
            $this->common->insert_data($agency_created_template_data, 'agency_created_template');
            $lastinsert_actid = $this->db->insert_id();
    
            // add agency number list details in agency call table
            if (!empty($template_number_list))
            {
                for ($k = 0; $k < count($template_number_list); $k++)
                {
                    $admintemplatenumber_data = array(
                        'agency_id' => $agency_id,
                        'template_id' => $template_id,
                        'number_id' => $template_number_list[$k],
                        'template_number_active' => 'Enable',
                        'act_id' => $lastinsert_actid,
                        'createddate' => date('Y-m-d H:i:s'),
                        'type' => 'CALL'
                    );
                 // echo "<pre>"; print_r($admintemplatenumber_data);
                    $this->common->insert_data($admintemplatenumber_data, 'template_number');            
                }     
            }
            if (!empty($template_smsnumber_list))
            {
                for ($k = 0; $k < count($template_smsnumber_list); $k++)
                {
                    $admintemplatenumber_data = array(
                        'agency_id' => $agency_id,
                        'template_id' => $template_id,
                        'number_id' => $template_smsnumber_list[$k],
                        'template_number_active' => 'Enable',
                        'createddate' => date('Y-m-d H:i:s'),
                        'act_id' => $lastinsert_actid,
                        'type' => 'SMS'
                    );
                 // echo "<pre>"; print_r($admintemplatenumber_data);
                    $this->common->insert_data($admintemplatenumber_data, 'template_number');            
                }
                $this->session->set_flashdata('success', 'Template number inserted successfully.');
                redirect('admintemplatenumber', 'refresh');
                
            }
            else
            {
                 
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('admintemplatenumber', 'refresh');
            }
    }

    public function delete($actid = NULL)
    {
        if ($actid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('admintemplatenumber', 'refresh');
        }
        else
        {
            // $templateid = $this->input->post('template_id');
            $actid = $this->input->post('act_id');
            // print_r($actid); die;
            // $this->admintemplatenumbers->delete_code_from_agency_created_template($templateid,$agencyid);
            $this->admintemplatenumbers->delete_code_from_agency_created_template($actid);
            // $this->admintemplatenumbers->delete_dt_from_templatenumber($templateid,$agencyid)
            if($this->admintemplatenumbers->delete_dt_from_templatenumber($actid))
            // if ($this->common->delete_data('template_number', 'template_id', $templatid))
            {
                $this->session->set_flashdata('success', 'Template number deleted successfully.');
                redirect('admintemplatenumber', 'refresh');
            }
           
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('admintemplatenumber', 'refresh');
            }
        }
    }

    public function edit($actid = NULL,$agencyid = NULL,$templateid = NULL)
    {
        /*echo "actid"; print_r(base64_decode($actid));
        echo "agencyid"; print_r($agencyid);
        echo "templateid"; print_r($templateid); die;*/
        if ($actid == NULL || $agencyid == NULL || $templateid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('admintemplatenumber', 'refresh');
        }
        else
        {
           /* $templateid = base64_decode($templateid);
            $agencyid = base64_decode($agencyid)*/;
            
            $actid = base64_decode($actid);
            $agencyid = base64_decode($agencyid);
            $templateid = base64_decode($templateid);
            // print_r($templateid); die;
            /*$this->data['admintemplatenumbers'] = $this->common->select_database_id($tablename = 'template_number', $columnname = 'template_id', $columnid = $templateid, $data = '*');*/

            $this->data['admintemplatenumbers'] = $this->common->select_database_id($tablename = 'template_number', $columnname = 'act_id', $columnid = $actid, $data = '*');

            // echo "<pre>"; print_r($this->data['admintemplatenumbers']); die;

            $this->data['agency_data'] = $this->admintemplatenumbers->get_agency_data_for_admin();

            /*$this->data['admintemplatenumber_data'] = $this->admintemplatenumbers->get_templateid_from_temp_and_agency($templateid,$agencyid);*/
          
            $this->data['admintemplatenumber_data'] = $this->admintemplatenumbers->get_templateid_from_temp_and_agency($actid,$agencyid);
 
            $this->data['temp_dt'] = $this->admintemplatenumbers->get_all_template_ids();
            
          /*  $a = array_column($this->data['temp_dt'], 'templateids');
            
            if($a[0] == null)
            {
                $a = array(0);
            }*/
            
/*            $this->data['agencyid'] = $agencyid;
            $this->data['templateid'] = $templateid;
*/
            /*$this->data['numberids_array'] = $this->admintemplatenumbers->numberids_from_templateid_and_agency_id($agencyid,$templateid); */

            $this->data['numberids_array'] = $this->admintemplatenumbers->numberids_from_templateid_and_agency_id($actid,$agencyid,$templateid); 

            /*$this->data['template_title'] = $this->admintemplatenumbers->get_template_title($agencyid,$templateid); */

            $this->data['template_title'] = $this->admintemplatenumbers->get_template_title($actid); 
            // echo "<pre>"; print_r($this->data['template_title']); die;

            // $this->data['template_data'] = $this->admintemplatenumbers->get_templates_from_agency_id($agencyid,$templateid);

            $this->data['template_data'] = $this->admintemplatenumbers->get_templates_from_agency_id($actid,$agencyid,$templateid);
            $this->data['number_data'] = $this->admintemplatenumbers->get_number_data_with_join($agencyid);
            $this->data['smsnumber_data'] = $this->admintemplatenumbers->get_smsnumber_data_with_join($agencyid);
           
            $this->load->view('admintemplatenumber/edit', $this->data);
        }
    }

//Updating the record
    public function update()
    {
        if ($this->input->post('template_id'))
        {
            $templatetitle = $this->input->post('title');
            $actid = $this->input->post('actid'); // Agency created template table id
            $templateid = $this->input->post('template_id');
            $agencyid = $this->input->post('agency_id');
            // print_r($templateid); die;
            $agency_numbers_list = $this->input->post('template_number_list');
            $agency_smsnumber_list = $this->input->post('template_smsnumber_list');
            
             // print_r($actid); die;
            $agency_created_template_data = array(
                'title' => $templatetitle,
                'modified_date' => date('Y-m-d H:i:s')
            );
                 // echo "<pre>"; print_r($admintemplatenumber_data);
            /*$this->common->update_data_multiple($agency_created_template_data, "agency_created_template", "agency_id", $agencyid, "template_id", $templateid);*/

            $this->common->update_data($agency_created_template_data, "agency_created_template", "actid", $actid);            
            if (!empty($agency_numbers_list))
            {
                /*$this->admintemplatenumbers->delete_data_from_templatenumber($templateid,$agencyid);*/
                $this->admintemplatenumbers->delete_data_from_templatenumber($actid,$agencyid);
                // $this->common->delete_data($tablename='template_number', $columnname='agency_id', $templateid);
                for ($k = 0; $k < count($agency_numbers_list); $k++)
                {
                    $admintemplatenumber_data = array(
                        'agency_id' => $agencyid,
                        'number_id'=>$agency_numbers_list[$k],
                        'template_id' => $templateid,
                        'template_number_active' => 'Enable',
                        'createddate' => date('Y-m-d H:i:s'),
                        'act_id' => $actid,
                        'type' => 'CALL'
                    );
                   
                    $this->common->insert_data($admintemplatenumber_data, $tablename='template_number');
                }
            }
            
            if (!empty($agency_smsnumber_list))
            {
              /*  echo "hello"; print_r(count($agency_smsnumber_list)); 
                print_r($templateid); 
                print_r($agencyid); die;*/
                // $this->admintemplatenumbers->delete_smsdata_from_templatenumber($templateid,$agencyid);
                $this->admintemplatenumbers->delete_smsdata_from_templatenumber($actid,$agencyid);
                for ($k = 0; $k < count($agency_smsnumber_list); $k++)
                {
                    $admintemplatenumber_data = array(
                        'agency_id' => $agencyid,
                        'number_id'=>$agency_smsnumber_list[$k],
                        'template_id' => $templateid,
                        'template_number_active' => 'Enable',
                        'createddate' => date('Y-m-d H:i:s'),
                        'act_id' => $actid,
                        'type' => 'SMS'
                    );
                   
                    $this->common->insert_data($admintemplatenumber_data, $tablename='template_number');
                
                }
                $this->session->set_flashdata('success', 'Template number  updated successfully.');
                redirect('admintemplatenumber', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('admintemplatenumber', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('admintemplatenumber', 'refresh');
        }
    }

    public function viewmodal($mode, $type_admintemplatenumber_active, $id, $sid)
    {
        $data['type_status'] = $type_admintemplatenumber_active;
        $data['mode'] = $mode;
        $data['id'] = $id;
        $data['sid'] = $sid;

        //Below actid is used for template number module only
        $data['actid'] = $type_admintemplatenumber_active;
        echo $this->load->view('admintemplatenumber/modal', $data, true);
    }


    public function mobileview()
    {
        // echo base_url();die;
        $templateid = $this->input->post('templateid');
        $this->data['templateview_data'] = $this->admintemplatenumbers->get_template_view_for_mobile($templateid);
        $templatemobileview = $this->data['templateview_data'][0]['template_mobile_view'];
        $gifpath = $this->config->item('gif_path');
        $url = base_url().$gifpath;
    //  ec2-3-19-38-126.us-east-2.compute.amazonaws.com
        $templatemobileview = str_replace("%url%",$url,$templatemobileview);
        print_r($templatemobileview);
    }
    
    public function changestatus()
    {
        $admintemplatenumberid = $this->input->post('admintemplatenumber_id');
        $admintemplatenumber_active = $this->input->post('admintemplatenumber_active');
        $status_msg = $this->input->post('status_msg');

        // print_r($this->input->post(NULL,TRUE));die();
        if (isset($this->session->userdata['devclick_admin']))
        {
            if (!$admintemplatenumberid)
            {
                $this->session->set_flashdata('message', 'Specified id not found.');
                redirect('admintemplatenumber', 'refresh');
            }
            if ($this->common->update_data(array('admintemplatenumber_active' => $admintemplatenumber_active), 'agency_number', 'admintemplatenumber_id', (int) $admintemplatenumberid))
            {
                $admintemplatenumberdata = $this->common->select_database_id('agency_number', 'admintemplatenumber_id', (int) $admintemplatenumberid, '*');
                /*
                  //$app_name_setting = $this->common->select_database_id('setting','AppSettingID',1);
                  $app_name = $this->common->get_setting_value(1);
                  $app_mail = $this->common->get_setting_value(6);
                  $this->load->library('email');
                  //Loading E-mail config file
                  $this->config->load('email', TRUE);
                  $this->cnfemail = $this->config->item('email');
                  $this->email->initialize($this->cnfemail);
                  $this->email->from($app_mail, $app_name);
                  $this->email->to($admintemplatenumberdata[0]['admintemplatenumber_email']);
                  if($admintemplatenumber_active =="Disable"){
                  $this->email->subject('Rest : admintemplatenumber Disable');
                  }
                  else
                  {
                  $this->email->subject('rest : admintemplatenumber Enable');
                  }
                  $mail_body = "<p>Hello&nbsp; " . $admintemplatenumberdata[0]['admintemplatenumber_name'] . ",</p>

                  <table>
                  <tbody>
                  <tr>
                  <td> Email</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $admintemplatenumberdata[0]['admintemplatenumber_email'] . "</td>
                  </tr>

                  <tr>
                  <td> Message</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $status_msg . ".</td>
                  </tr>

                  </tbody>
                  </table>
                  <p>
                  <span>If you have any question or trouble, please contact an app administrator.</span></p>
                  <p>&nbsp;</p>
                  <p>Regards,<br/>
                  " . $app_name . " Team.</p>";

                  //echo $mail_body;die();
                  $this->email->message($mail_body);

                  $this->email->send(); */

                $this->session->set_flashdata('success', 'Admin template number status changed to ' . $admintemplatenumber_active . '.');
                redirect('admintemplatenumber', 'refresh');
            }
            else
            {
             
                $this->session->set_flashdata('message', 'There is error in updating admin templatenumber status.Try later!');
                redirect('admintemplatenumber', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Something went wrong.Please try later!');
            redirect('dashboard', 'refresh');
        }
    }




    // This method is for get template data by agencyid
    public function get_template_data()
    {
        $agencyid = $this->input->post('agencyid');
        $this->data['templates'] = $this->admintemplatenumbers->get_templatedata_for_admin($agencyid);
        // print_r($this->data['templates']); die;
        echo json_encode($this->data['templates']);
    }
    
    public function get_number_data()
    {
        $agencyid = $this->input->post('agencyid');
        // print_r($agencyid); die;
         $this->data['numbers'] = $this->admintemplatenumbers->get_numberdata_for_admin($agencyid);
         echo json_encode($this->data['numbers']);
    }
    
    public function get_smsnumber_data()
    {
        $agencyid = $this->input->post('agencyid');
        //  print_r($agencyid); die;
        $this->data['smsnumbers'] = $this->admintemplatenumbers->get_smsnumberdata_for_admin($agencyid);
        echo json_encode($this->data['smsnumbers']);
    }
   


    // This function is used to generate shortcode for short url
    
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

/* End of file admintemplatenumber.php */
/* Location: ./application/controllers/admintemplatenumber.php */
