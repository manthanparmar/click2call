<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Webservice_model extends CI_Model {

//for login user
    function authenticate($email, $password)
    {

        $this->db->select('user_email,user_password');
        $this->db->where(array('user_email' => $email));
        $this->db->where(array('user_password' => $password));
        $user_detail = $this->db->get('user');
        $num_rows = $user_detail->num_rows();

        if ($num_rows >= 1)
        {
            return $num_rows;
        }
        else
        {
            return false;
        }
    }

    function loggedin_detail($email)
    {
        $this->db->select('user_id,user_email,name,user_image');
        $this->db->from('user');
        $this->db->where(array('user_email' => $email));
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function check_user_avalibility($email, $jeweler_id)
    {
        $this->db->where('user_email', $email);
        $this->db->where('jeweler_id', $jeweler_id);
        $this->db->where('user_active !=', 'Delete');
        $query = $this->db->get('user');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    function check_user_avalibility_by_social_id($social_id, $jeweler_id)
    {
        $this->db->where('user_social_id', $social_id);
        $this->db->where('jeweler_id', $jeweler_id);
        $this->db->where('user_active !=', 'Delete');
        $query = $this->db->get('user');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    function insert_data($data)
    {
        $query = $this->db->insert('user', $data);
        $num = $this->db->affected_rows();
        return $num;
    }

    function check_email($mail)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where(array('user_email' => $mail));
        $query = $this->db->get();
        $num = $query->num_rows();
        return $num;
    }

    function check_username($username)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where(array('username' => $username));
        $query = $this->db->get();
        $num = $query->num_rows();
        return $num;
    }

    function check_mail($mail)
    {
        $this->db->select('name,user_email,user_password');
        $this->db->from('user');
        $this->db->where(array('user_email' => $mail));
        $query = $this->db->get();
        $result = $query->result_array();
        if ($query->num_rows() > 0)
        {
            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    public function get_userdata($email)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_email', $email);
        $result = $this->db->get();
        return $result->result_array();
    }

    function editprofile($tablename, $user_data, $fieldname, $fieldvalue)
    {

        $this->db->where($fieldname, $fieldvalue);
        $this->db->update($tablename, $user_data);
        $num = $this->db->affected_rows();
        return $num;
    }

// Update on 02/02/2016

    public function user_status($id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where(array('user_id' => $id, 'user_active' => 'Enable'));
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    function get_user_detail($userid)
    {

        $this->db->select('u.user_id,u.name');
        $this->db->from('user u');
        $this->db->where(array('u.user_id' => $userid));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_profile($userid)
    {
        $this->db->select('name,user_image');
        $this->db->from('user');
        $this->db->where(array('user_id' => $userid));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_beacon($jeweler_id)
    {
        $this->db->select('b.beacon_id,b.beacon_minor,b.beacon_major,b.beacon_active,b.beacon_status,d.dept_id');
        $this->db->from('beacon b');
        $this->db->join('department_beacon db', 'b.beacon_id = db.beacon_id', 'right');
        $this->db->join('department d', 'd.dept_id = db.dept_id', 'left');
        $this->db->where('b.beacon_jeweler_id', $jeweler_id);
        $this->db->where('b.beacon_active', 'Enable');
        $this->db->group_by('b.beacon_id');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_product_beacon($jeweler_id)
    {
        $this->db->select('b.beacon_id,b.beacon_minor,b.beacon_major,b.beacon_active,b.beacon_status,p.pro_id');
        $this->db->from('beacon b');
        $this->db->join('product_beacon pb', 'b.beacon_id = pb.beacon_id', 'right');
        $this->db->join('product p', 'p.pro_id = pb.pro_id', 'left');
        $this->db->where('b.beacon_jeweler_id', $jeweler_id);
        $this->db->where('b.beacon_active', 'Enable');
        $this->db->group_by('b.beacon_id');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_department($dept_id)
    {
        $this->db->select('dept_id,jeweler_id,dept_name,dept_image,dept_active');
//        $this->db->from('product p');
        $this->db->from('department');
        $this->db->where('dept_id', $dept_id);
        $this->db->where('dept_active', 'Enable');
        $query = $this->db->get();
//     echo $this->db->last_query();
        $result = $query->result_array();
        return $result;
    }

    function get_all_product($dept_id)
    {
        $this->db->select('pro_id,jeweler_id,dept_id,pro_name,pro_SKU,pro_price,pro_loyal_price,pro_desc,pro_video_url,pro_active,pro_has_diamond,pro_diamond_name,pro_diamond_code');
        $this->db->from('product');
        $this->db->where('dept_id', $dept_id);
        $this->db->where('pro_active', 'Enable');
        $query = $this->db->get();
//     echo $this->db->last_query();
        $result = $query->result_array();
        return $result;
    }

    function get_all_product_images($product_id)
    {

        $this->db->select("CONCAT('" . base_url() . $this->config->item('product_upload_path') . "',pi.pro_image ) as pro_image");
        $this->db->from('product_image pi');
        $this->db->join('product p', 'p.pro_id = pi.pro_id', 'left');
        $this->db->where('pi.pro_id', $product_id);
        $this->db->where('p.pro_active', 'Enable');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_product_diamond_images($product_id)
    {

        $this->db->select("CONCAT('" . base_url() . $this->config->item('product_diamond_upload_path') . "',pdi.pro_diamond_image ) as pro_diamond_image");
        $this->db->from('product_diamond_image pdi');
        $this->db->join('product p', 'p.pro_id = pdi.pro_id', 'left');
        $this->db->where('pdi.pro_id', $product_id);
        $this->db->where('p.pro_active', 'Enable');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_product_size($product_id)
    {
        $this->db->select('ps.pro_size_id,ps.pro_size');
        $this->db->from('product_size ps');
        $this->db->join('product p', 'p.pro_id = ps.pro_id', 'left');
        $this->db->where('ps.pro_id', $product_id);
        $this->db->where('p.pro_active', 'Enable');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_product_diamond_attributes($product_id)
    {
        $this->db->select('pda.pro_diamond_key,pda.pro_diamond_value');
        $this->db->from('product_diamond_attributes pda');
        $this->db->join('product p', 'p.pro_id = pda.pro_id', 'left');
        $this->db->where('pda.pro_id', $product_id);
        $this->db->where('p.pro_active', 'Enable');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_all_products()
    {
        $this->db->from('product');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_product($product_id)
    {
        $this->db->select('pro_id,jeweler_id,dept_id,pro_name,pro_SKU,pro_price,pro_loyal_price,pro_desc,pro_video_url,pro_active,pro_has_diamond,pro_diamond_name,pro_diamond_code');
        $this->db->from('product');
        $this->db->where('pro_id', $product_id);
        $this->db->where('pro_active', 'Enable');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_wishlist_data($user_id)
    {
        $this->db->select('pw.pro_id,p.pro_name,p.pro_price,p.pro_active');
        $this->db->from('product_wishlist pw');
        $this->db->join('product p', 'pw.pro_id = p.pro_id', 'left');
        $this->db->where('pw.user_id', $user_id);
        $this->db->where('p.pro_active', 'Enable');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

//    public function get_advertise1()
//    {
////        $this->db->select('ja.jeweler_add_id,ja.jeweler_id,ja.jeweler_add_impression,ja.jeweler_add_click,ja.jeweler_add_total_click,ja.jeweler_add_total_impression,ja.start_date,ja.end_date,ja.jeweler_add_active,adv.jeweler_adv_text,adv.jeweler_adv_url,adv.jeweler_adv_image,');
//        $this->db->from('jeweler_advertise ja');
//        $this->db->join('jeweler_adv adv', 'adv.jeweler_add_id = ja.jeweler_add_id', 'left');
//        $this->db->where('ja.jeweler_add_active', 'Enable');
////        $this->db->where('Date(ja.end_date)', date('Y-m-d'));
//        $this->db->order_by('ja.jeweler_add_id', 'RANDOM');
//        $this->db->limit(1);
//        $query = $this->db->get();
//        $result = $query->result_array();
//        return $result;
//    }
//    public function get_advertise2()
//    {
//        $date = date('Y-m-d');
//        $this->db->select('ja.jeweler_add_id,ja.jeweler_id,ja.jeweler_add_impression,ja.jeweler_add_click,ja.jeweler_add_total_click,ja.jeweler_add_total_impression,ja.start_date,ja.end_date,ja.jeweler_add_active,adv.jeweler_adv_text,adv.jeweler_adv_url,adv.jeweler_adv_image,adv.jeweler_adv_frequency,(select COUNT(jai.jeweler_adv_imp_id) from jeweler_adv_impression jai ) as total_frequency');
//        $this->db->from('jeweler_advertise ja');
//        $this->db->join('jeweler_adv adv', 'adv.jeweler_add_id = ja.jeweler_add_id', 'left');
////        $this->db->join('jeweler_adv_impression jai', 'adv.jeweler_adv_id = jai.jeweler_adv_id', 'left');
//        $this->db->where("( ja.jeweler_add_active = 'Enable' AND Date(ja.end_date) >= $date AND ja.jeweler_add_click > ja.jeweler_add_total_click AND ja.jeweler_add_impression > ja.jeweler_add_total_impression AND adv.jeweler_adv_frequency > jai.total_frequency )");
////        $this->db->where("( ja.jeweler_add_active = 'Enable' AND Date(ja.end_date) >= $date AND ja.jeweler_add_click > ja.jeweler_add_total_click AND ja.jeweler_add_impression > ja.jeweler_add_total_impression AND jeweler_adv_frequency > (select COUNT(jai.jeweler_adv_imp_id) as total from 'jeweler_adv_impression' where avi.jeweler_adv_id = adv.jeweler_adv_id )");
//        $this->db->order_by('ja.jeweler_add_id', 'RANDOM');
//        $this->db->limit(1);
//        $query = $this->db->get();
//        $result = $query->result_array();
//        return $result;
//    }
    public function get_advertise()
    {
        $date = date('Y-m-d');
        $query = $this->db->query("SELECT `ja`.`jeweler_add_id`, `ja`.`jeweler_id`, `ja`.`jeweler_add_impression`, `ja`.`jeweler_add_click`, `ja`.`jeweler_add_total_click`, `ja`.`jeweler_add_total_impression`, `ja`.`start_date`, `ja`.`end_date`, `ja`.`jeweler_add_active`, `adv`.`jeweler_adv_id`,`adv`.`jeweler_adv_text`, `adv`.`jeweler_adv_url`, CONCAT('" . base_url() . $this->config->item('advretise_main_upload_path') . "',`adv`.`jeweler_adv_image`) as jeweler_adv_image, `adv`.`jeweler_adv_frequency`
FROM (`jeweler_advertise` ja)
LEFT JOIN `jeweler_adv` adv ON `adv`.`jeweler_add_id` = `ja`.`jeweler_add_id`
WHERE ( ja.jeweler_add_active = 'Enable' AND Date(ja.end_date) >= $date AND ja.jeweler_add_click > ja.jeweler_add_total_click AND ja.jeweler_add_impression > ja.jeweler_add_total_impression AND adv.jeweler_adv_frequency >= (select COUNT(*) from `jeweler_adv_impression` jai where jai.jeweler_adv_id = adv.jeweler_adv_id ))
ORDER BY  RAND()
LIMIT 1");
//        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function check_subscription_plan($jeweler_id)
    {
        $this->db->select('sub_id');
        $this->db->from('jeweler_subscription');
        $this->db->where('jeweler_sub_active', 'Enable');
        $this->db->where('Date(jeweler_sub_expiry_date) >', date('Y-m-d'));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

// ( select * from jeweler_order_product jop where jop.order_id = jo.order_id) as product_data'
    public function get_order_detail($order_id)
    {
        $this->db->select('jo.order_id,jo.order_transactionid,jo.order_amount,jo.payment_type,jo.order_payment_status,jo.createddate');
        $this->db->from('jeweler_order jo');
        $this->db->where('jo.order_id', $order_id);
//        $query = $this->db->query("SELECT `jo`.`order_id`, `jo`.`order_transactionid`, `jo`.`order_amount`, `jo`.`payment_type`, `jo`.`order_payment_status`, `jo`.`createddate`,( select * from jeweler_order_product jop where jop.order_id = jo.order_id) as `product_data`
//FROM (`jeweler_order` jo)
//WHERE `jo`.`order_id` =  '8'");
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_product_detail($order_id)
    {
        $this->db->select('jop.*,p.pro_name');
        $this->db->from('jeweler_order_product jop');
        $this->db->join('product p', 'p.pro_id = jop.pro_id', 'left');
        $this->db->where('jop.order_id', $order_id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_payment_detail($jeweler_id, $type)
    {
        if ($type == "Paypal")
        {
            $this->db->select('jp.payment_type,jp.payment_active,jpd.base_currency,jpd.secure_key');
            $this->db->from('jeweler_payment jp');
            $this->db->join('jeweler_paypaldetail jpd', 'jpd.jeweler_payment_id = jp.jeweler_payment_id', 'left');
            $this->db->where('jp.jeweler_id', $jeweler_id);
            $this->db->where('jp.payment_type', $type);
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }
        elseif ($type == 'Ccavenue')
        {
            $this->db->select('jp.payment_type,jp.payment_active,jcd.base_currency,jcd.secret_key,jcd.hashURL');
            $this->db->from('jeweler_payment jp');
            $this->db->join('jeweler_ccavenuedetail jcd', 'jcd.jeweler_payment_id = jp.jeweler_payment_id', 'left');
            $this->db->where('jp.jeweler_id', $jeweler_id);
            $this->db->where('jp.payment_type', $type);
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }
        else
        {
            return array();
        }
    }

    public function get_special_offer($user_id)
    {
        $date = date('Y-m-d');
        $this->db->select('percentage');
        $this->db->from('user_quick_offer');
        $this->db->where('user_id', $user_id);
        $this->db->where('Date(createddate)', $date);
        $this->db->order_by('quick_offer_id', 'DESC');
        $this->db->limit('1');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

}
