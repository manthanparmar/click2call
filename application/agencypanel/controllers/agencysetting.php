<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agencysetting extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('agency_admin'))
        {
//If no session, redirect to login page
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $sessionarray = $this->session->userdata('agency_admin');
        $agencyname = $sessionarray['agency_name'];
        
        
//Setting Page Title and Comman Variable
        $this->data['title'] = 'Administrator Dashboard';
        $this->data['section_title'] = 'Setting';
        $this->data['site_name'] = $agencyname;
        $this->data['site_url'] = $agencyname;

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        $this->load->model('common');
    }

    public function index()
    {
        $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];
        $this->data['agencysettings'] = $this->common->select_database_id('agency_setting', 'agency_id', $agencyid, $data = '*');
        // $this->data['total'] = $this->common->get_count_of_table('setting');
//Loading View File
        $this->load->view('agencysetting/index', $this->data);
    }

    public function view($settingid = NULL)
    {

        if ($settingid == NULL)
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('setting', 'refresh');
        }
        else
        {
            $this->data['settings'] = $this->common->select_database_id('setting', 'settingid', (int) $settingid, '*');
            //Loading View File
            $this->load->view('agencysetting/view', $this->data);
        }
    }

    public function edit($settingid)
    {
        $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];
        
        if (!$settingid)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('setting', 'refresh');
        }

        $this->data['agencysettings'] = $this->common->select_database_id('agency_setting', 'agency_id', (int) $agencyid, '*');
       
        if (count($this->data['agencysettings']) > 0)
        {
            $this->load->view('agencysetting/edit', $this->data);
        }
        else
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencysetting', 'refresh');
        }
        // echo "<pre>";print_r($this->data['settings']); die(); 
//Loading View File
    }

//Updating the record
    public function update()
    {
        if ($this->input->post('agency_id'))
        {
            $agencyid = base64_decode($this->input->post('agency_id'));
            $agencyname = $this->input->post('agency_name');
            $agencyemail = $this->input->post('agency_email');
            $data = array();
            
            $data = array(
                'agency_name' => $this->input->post('agency_name'),
                'agency_email' => $this->input->post('agency_email'),
                'modifieddate' => date('Y-m-d H:i:s')
            );
            
//            echo "<pre>";print_r($data);die();
            if ($this->common->update_data($data, 'agency_setting', 'agency_id', (int) $agencyid))
            {
                $this->session->set_flashdata('success', $agencyname . ' updated successfully.');
                redirect('agencysetting', 'refresh');
            }
            else
            {
                $log->warn("Error while updating record.");
                $this->session->set_flashdata('message', $agencyname . ' not updated successfully.');
                redirect('agencysetting', 'refresh');
            }
        }
        else
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencysetting', 'refresh');
        }
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
