<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class templatenumber extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/templatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('agency_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $this->load->model('templatenumbers');
        
        $sessionarray = $this->session->userdata('agency_admin');
        $agencyname = $sessionarray['agency_name'];

//Setting Page Title and Comman Variable
        $this->data['title'] = 'templatenumber';
        $this->data['section_title'] = 'templatenumber';
        $this->data['site_name'] = $agencyname;
        $this->data['site_url'] = $agencyname;

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->library('upload');
        $this->load->model('common');
    }

    public function index()
    {
        $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];
        
        $this->data['agencyid'] = $agencyid;
       // $this->data['templatenumbers'] = $this->templatenumbers->get_template_number_data();
        $this->data['templatenumbers'] = $templatenumbers = $this->templatenumbers->get_template_with_join($agencyid);

        for($i=0;$i<count($templatenumbers);$i++)
        {
            $code = $this->templatenumbers->get_code_by_angecy_template_id($agencyid,$templatenumbers[$i]['template_id']);

            // $code_new = $this->templatenumbers->get_code_by_angecy_template_id_new($agencyid,$templatenumbers[$i]['template_id']);

            $code_new = $this->templatenumbers->get_code_by_angecy_template_id_new($agencyid,$templatenumbers[$i]['act_id']);

            // print_r($code_new); die;
            $template_number = $this->templatenumbers->get_number_by_angecy_template_id($agencyid,$templatenumbers[$i]['template_id']);

            $template_call_number = $this->templatenumbers->get_call_number_by_angecy_template_id($agencyid,$templatenumbers[$i]['template_id'],$templatenumbers[$i]['act_id']);

            $template_sms_number = $this->templatenumbers->get_sms_number_by_angecy_template_id($agencyid,$templatenumbers[$i]['template_id'],$templatenumbers[$i]['act_id']);

            /*$template_title = $this->templatenumbers->get_templatetitle_by_angecy_template_id($agencyid,$templatenumbers[$i]['template_id']);*/

            $template_title = $this->templatenumbers->get_templatetitle_by_angecy_template_id($templatenumbers[$i]['act_id']);

            //   echo "<pre>";print_r($code);die;
            $this->data['templatenumbers'][$i]['code'] = $code;
            $this->data['templatenumbers'][$i]['code_new'] = $code_new;
            // $this->data['templatenumbers'][$i]['numbers'] = $template_number;
            $this->data['templatenumbers'][$i]['callnumbers'] = $template_call_number;
            $this->data['templatenumbers'][$i]['smsnumbers'] = $template_sms_number;
            $this->data['templatenumbers'][$i]['title'] = $template_title;
            $this->data['templatenumbers'][$i]['actid'] = $templatenumbers[$i]['act_id'];
             
        }
        // echo "<pre>";print_r($this->data['templatenumbers']); die;
        $this->data['total'] = count($this->data['templatenumbers']);
        $this->load->view('templatenumber/index', $this->data);
    }

    public function add()
    {

        $agencydata = $this->session->userdata('agency_admin');
        $agencyid = $agencydata['agency_id'];
       
        $this->data['templatenumber_data'] = $this->common->select_database_id($tablename = 'template_number', $columnname = 'template_number_active', $columnid = 'Enable', $data = '*');
        // echo "<pre>";print_r( $this->data['templatenumber_data']);die;
        $this->data['temp_dt'] = $this->templatenumbers->get_all_template_ids();
        // echo "<pre>";print_r( $this->data['temp_dt']);die;
         
        $a = array_column($this->data['temp_dt'], 'templateids');
        if($a[0] == null)
        {
            $a = array(0);
        }
        
        // print_r($a); die;
       
        $used_template = $this->common->select_database_id($tablename = 'template_number', $columnname = 'agency_id', $agencyid , $data = 'template_id');
      
        $used_template_id = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($used_template)), 0);
        $this->data['template_data'] = $this->templatenumbers->get_templates_not_in($a[0]);
        $this->data['template_data'] = $this->common->get_all_assign_templates($agencyid,$used_template_id);
        
        // echo "<pre>";print_r( $this->data['template_data']);die;
        $this->data['number_data'] = $this->templatenumbers->get_number_data_with_join($agencyid);
        $this->data['smsnumber_data'] = $this->templatenumbers->get_smsnumber_data_with_join($agencyid);

        $this->load->view('templatenumber/add', $this->data);
    }

    function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version templatenumber 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function addtemplatenumber()
    {
        $session_data = $this->session->userdata['agency_admin'];
        $agencyid = $session_data['agency_id'];

            $templatetitle = $this->input->post('title');
            $template_id=  $this->input->post('template_id');
            $template_number_list = $this->input->post('template_number_list');
            $template_smsnumber_list = $this->input->post('template_smsnumber_list');
    

            $agency_created_template_data = array(
                'agency_id' => $agencyid,
                'template_id' => $template_id,
                'title' => $templatetitle,
                'code' => $this->generateRandomString('6'),
                'created_date' => date('Y-m-d H:i:s')
            );
           
            $this->common->insert_data($agency_created_template_data, 'agency_created_template');
            $lastinsert_actid = $this->db->insert_id();

            // add agency number list details in agency call table
            if (!empty($template_number_list))
            {
                for ($k = 0; $k < count($template_number_list); $k++)
                {
                    $templatenumber_data = array(
                        'agency_id' => $agencyid,
                        'template_id' => $template_id,
                        'number_id' => $template_number_list[$k],
                        'template_number_active' => 'Enable',
                        'act_id' => $lastinsert_actid,
                        'createddate' => date('Y-m-d H:i:s'),
                        'type' => 'CALL'
                    );
                 // echo "<pre>"; print_r($templatenumber_data);
                    $this->common->insert_data($templatenumber_data, 'template_number');            
                }
            }
            
            if (!empty($template_smsnumber_list))
            {
                for ($k = 0; $k < count($template_smsnumber_list); $k++)
                {
                    $templatenumber_data = array(
                        'agency_id' => $agencyid,
                        'template_id' => $template_id,
                        'number_id' => $template_smsnumber_list[$k],
                        'template_number_active' => 'Enable',
                        'act_id' => $lastinsert_actid,
                        'createddate' => date('Y-m-d H:i:s'),
                        'type' => 'SMS'
                    );
                 // echo "<pre>"; print_r($templatenumber_data);
                    $this->common->insert_data($templatenumber_data, 'template_number');            
                }
                
                $this->session->set_flashdata('success', 'Template number inserted successfully.');
                redirect('templatenumber', 'refresh');
            }
            else
            {
                 
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('templatenumber', 'refresh');
            }
    }

    public function delete($actid = NULL)
    {
        if ($actid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('templatenumber', 'refresh');
        }
        else
        {
            /*$templateid = $this->input->post('template_id');
            $agencyid = $this->input->post('agency_id');*/   
            $actid = $this->input->post('act_id'); 
            
            // print_r($agencyid); die;
        // $this->templatenumbers->delete_code_from_agency_created_template($templateid,$agencyid);
            $this->templatenumbers->delete_code_from_agency_created_template($actid);
            if($this->templatenumbers->delete_dt_from_templatenumber($actid))
            {
                $this->session->set_flashdata('success', 'Template number deleted successfully.');
                redirect('templatenumber', 'refresh');
            }
           
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('templatenumber', 'refresh');
            }
        }
    }

    public function edit($templateid = NULL,$act_id = NULL)
    {
        if ($templateid == NULL || $act_id == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('templatenumber', 'refresh');
        }
        else
        {
            $templateid = base64_decode($templateid);
            $actid = base64_decode($act_id);
            
            // print_r($templateid); die;
            /*$this->data['templatenumbers'] = $this->common->select_database_id($tablename = 'template_number', $columnname = 'template_id', $columnid = $templateid, $data = '*');*/

            $this->data['templatenumbers'] = $this->common->select_database_id($tablename = 'template_number', $columnname = 'act_id', $columnid = $actid, $data = '*');
             
            $agencydata = $this->session->userdata('agency_admin');
            $agencyid = $agencydata['agency_id'];

            /*$this->data['templatenumber_data'] = $this->templatenumbers->get_templateid_from_temp_and_agency($agencyid,$templateid);*/
            $this->data['templatenumber_data'] = $this->templatenumbers->get_templateid_from_temp_and_agency($agencyid,$actid);
            // echo "<pre>"; print_r($this->data['templatenumber_data']); die;
            $this->data['temp_dt'] = $this->templatenumbers->get_all_template_ids();
            
          /*  $a = array_column($this->data['temp_dt'], 'templateids');
            
            if($a[0] == null)
            {
                $a = array(0);
            }*/
          
            // $this->data['template_title'] = $this->templatenumbers->get_template_title($agencyid,$templateid); 

            $this->data['template_title'] = $this->templatenumbers->get_template_title($actid); 

             // print_r($this->data['template_title']); die;
            $this->data['numberids_array'] = $this->templatenumbers->numberids_from_templateid_and_agency_id($templateid,$agencyid,$actid);   
            $this->data['template_data'] = $this->templatenumbers->get_templates_from_agency_id($agencyid,$templateid,$actid);

            $this->data['number_data'] = $this->templatenumbers->get_number_data_with_join($agencyid);
            $this->data['smsnumber_data'] = $this->templatenumbers->get_smsnumber_data_with_join($agencyid);
            $this->load->view('templatenumber/edit', $this->data);
        }
    }

//Updating the record
    public function update()
    {
        if ($this->input->post('template_id'))
        {
            // $template_id = base64_decode($this->input->post('template_id'));
            $templatetitle = $this->input->post('title');
            $actid = $this->input->post('actid'); // Agency created template table id
            $template_id = $this->input->post('template_id');
             // print_r($template_id); die;
            $agency__data = $this->session->userdata('agency_admin');
            $agencyids = $agency__data['agency_id'];
            
            $agency_numbers_list = $this->input->post('template_number_list');
            $agency_smsnumbers_list = $this->input->post('template_smsnumber_list');

            //echo "hie"; print_r(count($agency_smsnumbers_list)); die;

            $agency_created_template_data = array(
                'title' => $templatetitle,
                'modified_date' => date('Y-m-d H:i:s')
            );
           /* $this->common->update_data_multiple($agency_created_template_data, "agency_created_template", "agency_id", $agencyids, "template_id", $template_id);*/

        $this->common->update_data($agency_created_template_data, "agency_created_template", "actid", $actid);

            if (!empty($agency_numbers_list))
            {
                // print_r($agencyids); die;
                // $this->templatenumbers->delete_data_from_templatenumber($template_id,$agencyids);
                $this->templatenumbers->delete_data_from_templatenumber($actid,$agencyids,$template_id);
                //$this->common->delete_data($tablename='template_number', $columnname='template_id', $template_id);
                for ($k = 0; $k < count($agency_numbers_list); $k++)
                {
                    $templatenumber_data = array(
                        'agency_id' => $agencyids,
                        'number_id'=>$agency_numbers_list[$k],
                        'template_id' => $template_id,
                        'template_number_active' => 'Enable',
                        'createddate' => date('Y-m-d H:i:s'),
                        'act_id' => $actid,
                        'type' => 'CALL'
                    );
                   
                    $this->common->insert_data($templatenumber_data, $tablename='template_number');
                
                }
            }
            if (!empty($agency_smsnumbers_list))
            {
                
                // $this->templatenumbers->delete_smsdata_from_templatenumber($template_id,$agencyids);
                $this->templatenumbers->delete_smsdata_from_templatenumber($actid,$agencyids,$template_id);
                //$this->common->delete_data($tablename='template_number', $columnname='template_id', $template_id);

                for ($k = 0; $k < count($agency_smsnumbers_list); $k++)
                {
                    $templatenumber_data = array(
                        'agency_id' => $agencyids,
                        'number_id'=>$agency_smsnumbers_list[$k],
                        'template_id' => $template_id,
                        'template_number_active' => 'Enable',
                        'createddate' => date('Y-m-d H:i:s'),
                        'act_id' => $actid,
                        'type' => 'SMS'
                    );
                   
                    $this->common->insert_data($templatenumber_data, $tablename='template_number');
                
                }
                $this->session->set_flashdata('success', 'Template number updated successfully.');
                redirect('templatenumber', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('templatenumber', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('templatenumber', 'refresh');
        }
    }

    public function viewmodal($mode, $type_templatenumber_active, $id, $sid)
    {
        /*print_r($mode); 
        echo "actid"; print_r(base64_decode($type_templatenumber_active)); 
        echo "templateid"; print_r(base64_decode($id));  
        echo "agencyid"; print_r(base64_decode($sid));   die;*/
        $data['type_status'] = $type_templatenumber_active;
        $data['mode'] = $mode;
        $data['id'] = $id;
        $data['sid'] = $sid;

    //Below actid is used for template number module only
        $data['actid'] = $type_templatenumber_active;
        echo $this->load->view('templatenumber/modal', $data, true);
    }


    public function mobileview()
    {
        $templateid = $this->input->post('templateid');
        $this->data['templateview_data'] = $this->templatenumbers->get_template_view_for_mobile($templateid);
        $templatemobileview = $this->data['templateview_data'][0]['template_mobile_view'];
         $gifpath = $this->config->item('gif_path');
          $url = base_url().$gifpath;
           $templatemobileview = str_replace("%url%",$url,$templatemobileview);
        print_r($templatemobileview);
    }
    
    public function changestatus()
    {
        $templatenumberid = $this->input->post('templatenumber_id');
        $templatenumber_active = $this->input->post('templatenumber_active');
        $status_msg = $this->input->post('status_msg');

        // print_r($this->input->post(NULL,TRUE));die();
        if (isset($this->session->userdata['agency_admin']))
        {
            if (!$templatenumberid)
            {
                $this->session->set_flashdata('message', 'Specified id not found.');
                redirect('templatenumber', 'refresh');
            }
            if ($this->common->update_data(array('templatenumber_active' => $templatenumber_active), 'agency_number', 'templatenumber_id', (int) $templatenumberid))
            {
                $templatenumberdata = $this->common->select_database_id('agency_number', 'templatenumber_id', (int) $templatenumberid, '*');
                /*
                  //$app_name_setting = $this->common->select_database_id('setting','AppSettingID',1);
                  $app_name = $this->common->get_setting_value(1);
                  $app_mail = $this->common->get_setting_value(6);
                  $this->load->library('email');
                  //Loading E-mail config file
                  $this->config->load('email', TRUE);
                  $this->cnfemail = $this->config->item('email');
                  $this->email->initialize($this->cnfemail);
                  $this->email->from($app_mail, $app_name);
                  $this->email->to($templatenumberdata[0]['templatenumber_email']);
                  if($templatenumber_active =="Disable"){
                  $this->email->subject('Rest : templatenumber Disable');
                  }
                  else
                  {
                  $this->email->subject('rest : templatenumber Enable');
                  }
                  $mail_body = "<p>Hello&nbsp; " . $templatenumberdata[0]['templatenumber_name'] . ",</p>

                  <table>
                  <tbody>
                  <tr>
                  <td> Email</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $templatenumberdata[0]['templatenumber_email'] . "</td>
                  </tr>

                  <tr>
                  <td> Message</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $status_msg . ".</td>
                  </tr>

                  </tbody>
                  </table>
                  <p>
                  <span>If you have any question or trouble, please contact an app administrator.</span></p>
                  <p>&nbsp;</p>
                  <p>Regards,<br/>
                  " . $app_name . " Team.</p>";

                  //echo $mail_body;die();
                  $this->email->message($mail_body);

                  $this->email->send(); */

                $this->session->set_flashdata('success', 'Agency number status changed to ' . $templatenumber_active . '.');
                redirect('templatenumber', 'refresh');
            }
            else
            {
             
                $this->session->set_flashdata('message', 'There is error in updating templatenumber status.Try later!');
                redirect('templatenumber', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Something went wrong.Please try later!');
            redirect('dashboard', 'refresh');
        }
    }

    public function email_exist($templatenumberid = NULL)
    {
//        print_r($this->input->post(NULL,TRUE));die();

        $templatenumbermail = $this->input->post('templatenumberemail');

        if ($templatenumbermail != '')
        {

            if ($templatenumberid == NULL)
            {
                if ($this->common->check_unique_avalibility('templatenumber', 'templatenumber_email', $templatenumbermail, 'templatenumber_active', 'Delete'))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
            else
            {
                if ($this->common->check_unique_avalibility('templatenumber', 'templatenumber_email', $templatenumbermail, 'templatenumber_id', (int) base64_decode($templatenumberid), array('templatenumber_active !=' => 'Delete')))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
        }
        else
        {
            $response = array('valid' => false, 'message' => 'Enter valid email.');
            echo json_encode($response);
            die();
        }

    }

    public function multipleEvent()
    {
        
        if ($this->input->post('event') == 'Enable')
        {
            $enableId = $this->input->post('check');
            if ($enableId != '' && $enableId != 0)
            {
                for ($i = 0; $i < count($enableId); $i++)
                {
                    $allId[] = $enableId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'templatenumber_active', $value = "Enable", 'templatenumber_id', 'templatenumber'))
                {
                    $this->session->set_flashdata('success', 'Agency number status changed to Enable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('templatenumber', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating templatenumber status. Try later');
                    redirect('templatenumber', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Enable. Try later!');
                redirect('templatenumber', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Disable')
        {
//check permission
// disable multiple item
            $disableId = $this->input->post('check');
            if ($disableId != '' && $disableId != 0)
            {
                for ($i = 0; $i < count($disableId); $i++)
                {
                    $allId[] = $disableId[$i];
                }

                if ($this->common->multipleEvent($allId, $status_columnname = 'templatenumber_active', $value = "Disable", 'templatenumber_id', 'templatenumber'))
                {
                    $this->session->set_flashdata('success', 'Agency number status changed to Disable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('templatenumber', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating templatenumber status. Try later');
                    redirect('templatenumber', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Disable. Try later!');
                redirect('templatenumber', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Delete')
        {

            $deleteId = $this->input->post('check');
            if ($deleteId != '' && $deleteId != 0)
            {
                for ($i = 0; $i < count($deleteId); $i++)
                {
                    $allId[] = $deleteId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'templatenumber_active', $value = "Delete", 'templatenumber_id', 'templatenumber'))
                {
                    $this->session->set_flashdata('success', 'Agency number deleted successfully.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('templatenumber', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in deleting templatenumber. Try later');
                    redirect('templatenumber', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Delete. Try later!');
                redirect('templatenumber', 'refresh');
            }
        }
    }




    // This function is used to generate shortcode for short url
    
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}

/* End of file templatenumber.php */
/* Location: ./application/controllers/templatenumber.php */
