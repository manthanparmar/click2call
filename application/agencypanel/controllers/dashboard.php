<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        if (!$this->session->userdata('agency_admin'))
        {
            //If no session, redirect to login page
            redirect('adminlogin', 'refresh');
        }
        
        $sessionarray = $this->session->userdata('agency_admin');
        $agencyname = $sessionarray['agency_name'];
        include('include.php');
        //Setting Page Title and Comman Variable
        $this->data['title'] = 'Administrator Dashboard';
        $this->data['section_title'] = 'Dashboard';
        $this->data['site_name'] = $agencyname;
        $this->data['site_url'] = $agencyname;
        $this->load->model('common');
        $this->load->model('dashboardmodel');
        $this->load->model('templatenumbers');


        //Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
        //Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
    }

    function getWeek($week, $year)
    {
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
        return $result;
    }

    public function index()
    {

        //$this->load->view('dashboard',$this->data);
        if (isset($this->session->userdata['agency_admin']))
        {
            
            $sessionarray = $this->session->userdata('agency_admin');
            $agencyid = $sessionarray['agency_id'];
            

            $from_date = date('Y-m-d H:i:s');
            $to_date = date('Y-m-d H:i:s');
            $calltype = '';    
           
            $this->data['from_date'] = $from_date;
            $this->data['to_date'] = $to_date;
            $this->data['calltype'] = $calltype;
            $this->data['agencyid'] = $agencyid;
            $this->data['filtered_data'] = $this->dashboardmodel->get_history_filter_data($from_date,$to_date,$agencyid,$calltype);


            $this->data['agency_data'] = $this->templatenumbers->get_agency_data();

            $this->data['templatenumbers'] = $this->templatenumbers->get_template_with_join($agencyid);
            $this->data['templates'] = $this->common->get_template_by_agency_id($agencyid);
        
            $this->data['total'] = count($this->data['templatenumbers']);
                $this->load->view('dashboard', $this->data);
            }
            else
            {
                redirect('adminlogin', 'refresh');
            }
    }
    

    function get_filtered_data()
    {
        $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];

        if(isset($_POST['submit']))
        {
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];
            $calltype = $_POST['calltype'];    
        }
        
        // print_r($calltype);die;

        $this->data['agency_data'] = $this->templatenumbers->get_agency_data();
        $this->data['from_date'] = $from_date;
        $this->data['to_date'] = $to_date;
        $this->data['calltype'] = $calltype;
        $this->data['agencyid'] = $agencyid;
        $this->data['filtered_data'] = $this->dashboardmodel->get_history_filter_data($from_date,$to_date,$agencyid,$calltype);

        
        $this->data['total_number'] = count($this->common->get_record('number', 'number_active !=', 'Delete'));
        $this->data['total_agency'] = count($this->common->get_record('agency', 'agency_active !=', 'Delete'));
        $this->data['total_agencynumber'] = count($this->common->get_record('agency_number', 'agencynumber_active !=', 'Delete'));
        $this->data['total_agencytemplate'] = count($this->common->get_record('agency_template', 'agencytemplate_active !=', 'Delete'));

       

        $this->load->view('dashboard', $this->data);
    }
    
    
    function get_template_click_data()
    {
        $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];
        
        $templateid = $this->input->post('templateid');
        $this->data['clickdata'] = $this->dashboardmodel->get_template_click($agencyid,$templateid);
        echo json_encode($this->data['clickdata']);
    }

  
    function get_click_data()
    {
        $type = $this->input->post('type');
        $click_template = $this->input->post('click_template');
        
        if ($type == 'week')
        {
            $user_data = $this->dashboardmodel->get_click('week',$click_template);
            // print_r($user_data); 
            $week_data = array();
            $today = date('Y-m-d');
            $week = date("W", strtotime($today));
            $year = date("Y", strtotime($today));
            $weekdates = $this->getWeek($week, $year);
            $startdate = $weekdates['start'];
            $enddate = $weekdates['end'];
            for ($w = 0; $w < 7; $w++)
            {
                $date = date('Y-m-d', strtotime("$w days", strtotime($startdate)));
                if (!empty($user_data))
                {
                    for ($wd = 0; $wd < count($user_data); $wd++)
                    {
                        // echo $date."**".$user_data[$wd]['createddate']; 
                        // echo "<br>";
                        if (isset($user_data[$wd]['createddate']) && $user_data[$wd]['createddate'] == $date)
                        {
                            if (strtotime($today) >= strtotime($date))
                            {
                                $week_data[$w]['Date'] = date('m-d-Y',strtotime($date));
                                $week_data[$w]['Visitors'] = $user_data[$wd]['click'];
                                break;
                            }
                        }
                        else
                        {
                            if (strtotime($today) >= strtotime($date))
                            {
                                $week_data[$w]['Date'] = date('m-d-Y',strtotime($date));
                                $week_data[$w]['Visitors'] = 0;
                            }
                        }
                    }
                }
                else
                {
                    if (strtotime($today) >= strtotime($date))
                    {
                        $week_data[$w]['Date'] = date('m-d-Y',strtotime($date));
                        $week_data[$w]['Visitors'] = 0;
                    }
                }
            }
            echo json_encode($week_data);
            die;
        }
        elseif ($type == 'year')
        {
            $today = date('Y-m-d');
            $user_data = $this->dashboardmodel->get_click('year',$click_template);
            $year_data = array();

            for ($y = 0; $y < 12; $y++)
            {
                if (!empty($user_data))
                {
                    for ($yd = 0; $yd < count($user_data); $yd++)
                    {
                        if (isset($user_data[$yd]['createddate']) && $user_data[$yd]['createddate'] == $y)
                        {
                            if (strtotime($today) >= strtotime(date("d-$y-Y")))
                            {
                                $year_data[$y]['Date'] = date('M', strtotime(date("d-$y-Y")));
                                $year_data[$y]['Visitors'] = $user_data[$yd]['click'];
                                break;
                            }
                        }
                        else
                        {
                            if (strtotime($today) >= strtotime(date("d-$y-Y")))
                            {
                                $year_data[$y]['Date'] = date('M', strtotime(date("d-$y-Y")));
                                $year_data[$y]['Visitors'] = 0;
                            }
                        }
                    }
                }
                else
                {
                    if (strtotime($today) >= strtotime(date("d-$y-Y")))
                    {
                        $year_data[$y]['Date'] = date('M', strtotime(date("d-$y-Y")));
                        $year_data[$y]['Visitors'] =0;
                    }
                }
            }
            echo json_encode($year_data);
            die;
        }
        elseif ($type == 'month')
        {
            $today = date('Y-m-d');
            $user_data = $this->dashboardmodel->get_click('month',$click_template);
            $month_array = array();
            $total_day = date('t', strtotime('this month'));
            for ($i = 0; $i < $total_day; $i++)
            {
                if (!empty($user_data))
                {
                    for ($j = 0; $j < count($user_data); $j++)
                    {
                        if (isset($user_data[$j]['createddate']) && $user_data[$j]['createddate'] == $i)
                        {
                            // $day = $i + 1;
                              $day = $i;
                            if (strtotime($today) >= strtotime(date("$day-m-Y")))
                            {
                                $month_array[$i]['Date'] = date("m-$day-Y");
                                $month_array[$i]['Visitors'] = $user_data[$j]['click'];
                                break;
                            }
                        }
                        else
                        {
                            // $day = $i + 1;
                            $day = $i;
                            if (strtotime($today) >= strtotime(date("$day-m-Y")))
                            {
                                $month_array[$i]['Date'] = date("m-$day-Y");
                                $month_array[$i]['Visitors'] = 0;
                            }
                        }
                    }
                }
                else
                {
                    // $day = $i + 1;
                    $day = $i;
                    if (strtotime($today) >= strtotime(date("$day-m-Y")))
                    {
                        $month_array[$i]['Date'] = date("m-$day-Y");
                        $month_array[$i]['Visitors'] = 0;
                    }
                }
            }

            echo json_encode($month_array);
            die;
        }
    }
    
    
    
    // CALL SMS GRAPH
    
    function get_callhistory_data()
    {
        $type = $this->input->post('type');
        $call_history = $this->input->post('call_history');
        
        
        if ($type == 'week')
        {
            $user_data = $this->dashboardmodel->get_call_history_data('week',$call_history);
            // print_r($user_data); 
            $week_data = array();
            $today = date('Y-m-d');
            $week = date("W", strtotime($today));
            $year = date("Y", strtotime($today));
            $weekdates = $this->getWeek($week, $year);
            $startdate = $weekdates['start'];
            $enddate = $weekdates['end'];
            for ($w = 0; $w < 7; $w++)
            {
                $date = date('Y-m-d', strtotime("$w days", strtotime($startdate)));
                if (!empty($user_data))
                {
                    for ($wd = 0; $wd < count($user_data); $wd++)
                    {
                        // echo $date."**".$user_data[$wd]['createddate']; 
                        // echo "<br>";
                        if (isset($user_data[$wd]['createddate']) && $user_data[$wd]['createddate'] == $date)
                        {
                            if (strtotime($today) >= strtotime($date))
                            {
                                $week_data[$w]['Date'] = date('m-d-Y',strtotime($date));
                                $week_data[$w][$call_history] = $user_data[$wd]['historyid'];
                                break;
                            }
                        }
                        else
                        {
                            if (strtotime($today) >= strtotime($date))
                            {
                                $week_data[$w]['Date'] = date('m-d-Y',strtotime($date));
                                $week_data[$w][$call_history] = 0;
                            }
                        }
                    }
                }
                else
                {
                    if (strtotime($today) >= strtotime($date))
                    {
                        $week_data[$w]['Date'] = date('m-d-Y',strtotime($date));
                        $week_data[$w][$call_history] = 0;
                    }
                }
            }
            echo json_encode($week_data);
            die;
        }
        elseif ($type == 'year')
        {
            $today = date('Y-m-d');
            $user_data = $this->dashboardmodel->get_call_history_data('year',$call_history);
            $year_data = array();

            for ($y = 0; $y < 12; $y++)
            {
                if (!empty($user_data))
                {
                    for ($yd = 0; $yd < count($user_data); $yd++)
                    {
                        if (isset($user_data[$yd]['createddate']) && $user_data[$yd]['createddate'] == $y)
                        {
                            if (strtotime($today) >= strtotime(date("d-$y-Y")))
                            {
                                $year_data[$y]['Date'] = date('M', strtotime(date("d-$y-Y")));
                                $year_data[$y][$call_history] = $user_data[$yd]['historyid'];
                                break;
                            }
                        }
                        else
                        {
                            if (strtotime($today) >= strtotime(date("d-$y-Y")))
                            {
                                $year_data[$y]['Date'] = date('M', strtotime(date("d-$y-Y")));
                                $year_data[$y][$call_history] = 0;
                            }
                        }
                    }
                }
                else
                {
                    if (strtotime($today) >= strtotime(date("d-$y-Y")))
                    {
                        $year_data[$y]['Date'] = date('M', strtotime(date("d-$y-Y")));
                        $year_data[$y][$call_history] = 0;
                    }
                }
            }
            echo json_encode($year_data);
            die;
        }
        elseif ($type == 'month')
        {
            $today = date('Y-m-d');
            $user_data = $this->dashboardmodel->get_call_history_data('month',$call_history);
            $month_array = array();
            $total_day = date('t', strtotime('this month'));
            for ($i = 0; $i < $total_day; $i++)
            {
                if (!empty($user_data))
                {
                    for ($j = 0; $j < count($user_data); $j++)
                    {
                        if (isset($user_data[$j]['createddate']) && $user_data[$j]['createddate'] == $i)
                        {
                            // $day = $i + 1;
                               $day = $i;
                            if (strtotime($today) >= strtotime(date("$day-m-Y")))
                            {
                                $month_array[$i]['Date'] = date("m-$day-Y");
                                $month_array[$i][$call_history] = $user_data[$j]['historyid'];
                                break;
                            }
                        }
                        else
                        {
                             //$day = $i + 1;
                             $day = $i;
                            if (strtotime($today) >= strtotime(date("$day-m-Y")))
                            {
                                $month_array[$i]['Date'] = date("m-$day-Y");
                                $month_array[$i][$call_history] = 0;
                            }
                        }
                    }
                }
                else
                {
                     // $day = $i + 1;
                     $day = $i;
                    if (strtotime($today) >= strtotime(date("$day-m-Y")))
                    {
                        $month_array[$i]['Date'] = date("m-$day-Y");
                        $month_array[$i][$call_history] = 0;
                    }
                }
            }

            echo json_encode($month_array);
            die;
        }
    }
    
    
    
   

    function time_elapsed_string($ptime)
    {

        $etime = strtotime(date('Y-m-d H:i:s')) - strtotime($ptime);

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

    function logout()
    {
        if (isset($this->session->userdata['agency_admin']))
        {
            $this->session->unset_userdata('agency_admin');
            $this->session->sess_destroy();
            redirect('adminlogin', 'refresh');
        }
        else
        {
            $this->session->unset_userdata('agency_admin');
            $this->session->sess_destroy();
            redirect('adminlogin', 'refresh');
        }
    }

    public function jeweler($utype = '')
    {
        //$this->load->view('dashboard',$this->data);
        if (isset($this->session->userdata['agency_admin']))
        {
            $this->data['user'] = $this->dashboardmodel->get_user_of_limit(8);
            $this->data['total_user'] = count($this->common->get_record('user', 'user_active !=', 'Delete'));
            $this->data['total_restaurant'] = count($this->common->get_record('jeweler', 'jeweler_active !=', 'Delete'));
            $this->data['total_beacon'] = count($this->common->get_record('beacon', 'beacon_active !=', 'Delete'));
            $this->data['total_subscription'] = count($this->common->get_record('subscription', 'sub_active !=', 'Delete'));

            if ($utype == "" || $utype == "weekly")
            {
                $getdate = strtotime(date("Y-m-d"));
                // calculate the number of days since Monday
                $dow = date('w', $getdate);
                $offset = $dow - 1;
                if ($offset < 0)
                {
                    $offset = 6;
                }
                // calculate timestamp for Monday and Sunday
                $monday = $getdate - ($offset * 86400);
                $sunday = $monday + (6 * 86400);

                // print dates for Monday and Sunday in the current week
                $startdate = date("Y-m-d", $monday);
                $enddate = date("Y-m-d", $sunday);

                $this->data['enddate'] = $enddate;
            }
            elseif ($utype == "monthly")
            {
                
            }
            elseif ($utype == "monthyear")
            {
                $this->data['syear'] = $this->input->post('syear');
                $this->data['smonth'] = $this->input->post('smonth');
            }
            $this->data['mindate'] = $this->dashboardmodel->get_jeweler_minimum_date();
            $this->data['type'] = 'jeweler';

            $this->load->view('dashboard', $this->data);
        }
        else
        {
            redirect('adminlogin', 'refresh');
        }
    }

}

/* End of file dashboard.php */
    /* Location: ./application/controllers/dashboard.php */
    
