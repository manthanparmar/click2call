<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Changepassword extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('agency_admin'))
        {
            redirect('adminlogin', 'refresh');
        }
        include('include.php');
        
         $sessionarray = $this->session->userdata('agency_admin');
        $agencyname = $sessionarray['agency_name'];

        $this->data['title'] = 'Administrator Dashboard';
        $this->data['section_title'] = 'Change Password';
        $this->data['site_name'] = $agencyname;
        $this->data['site_url'] = $agencyname;


        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->model('common');
    }

    public function index()
    {
        $admindata = $this->session->userdata('agency_admin');
        $agencyid = $admindata['agency_id'];
        $this->data['admin'] = $this->common->select_database_id('agency', 'agency_id', $agencyid, 'agency_email,agency_name');
        $this->load->view('changepassword', $this->data);
    }

    public function change()
    {
//        echo '<pre>';print_r($this->input->post(NULL, TRUE));die();
        // $log = Logger::getLogger(__CLASS__);
        $admindata = $this->session->userdata('agency_admin');
        $agencyid = $admindata['agency_id'];
        
        
        $oldpassword = $this->input->post('oldpassword');
        $newpassword = $this->input->post('newpassword');
        $confirmpass = $this->input->post('confirmpass');
        $agencyname = $this->input->post('agencyname');
        $agencyemail = $this->input->post('agencyemail');

        // print_r($agencyemail); die;
        if ($newpassword != $confirmpass)
        {
            $this->session->set_flashdata('message', 'New password and Confirm password must be same.');
            redirect('changepassword', 'refresh');
        }

        if ($this->common->check_unique_avalibility('agency', 'agency_password', $oldpassword))
        {
            if ($newpassword != '')
            {
                $data = array(
                    'agency_password' => $newpassword,
                    'agency_email' => $agencyemail,
                    'agency_name' => $agencyname,
                );
            }
            else
            {
                $data = array(
                    'agency_email' => $agencyemail,
                    'agency_password' => $agencyname
                );
            }

            if ($this->common->update_data($data, 'agency', 'agency_id', $agencyid))
            {

                $sess_array = array(
                    // 'id' => 0,
                    'agency_id' => $agencyid,
                    'agency_name' => $agencyname,
                    'username' => $agencyemail,
                    'type' => 'agency',
                );
                $this->session->set_userdata('agency_admin', $sess_array);

                $this->session->set_flashdata('success', 'Profile details updated successfully.');
                redirect('dashboard', 'refresh');
            }
            else
            {
                // $log->warn("Error while updating record.");
                $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                redirect('changepassword', 'refresh');
            }
        }
        else
        {
            // $log->warn("Error while updating record.");
            $this->session->set_flashdata('message', 'Old password is not match. Try Again.');
            redirect('changepassword', 'refresh');
        }
    }



}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
