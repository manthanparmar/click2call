<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adminlogin extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        if ($this->session->userdata('agency_admin')) {

            //If no session, redirect to login page
            //echo site_url();die();
            redirect('dashboard', 'refresh');
        }
        
        
        //Setting Page Title and Comman Variable
        $this->data['title'] = 'Administrator Log-in';
        $this->data['section_title'] = 'Administrator Log-in';
        $this->data['site_name'] = 'ASPL';
        $this->data['site_url'] = 'ASPL';

      
        //Load leftsidemenu and save in variable
        $this->data['topmenu'] = '';

        //Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
    }

    public function index() {
        
        
        // LOAD LIBRARIES
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        // LOAD HELPERS
        $this->load->helper('form');

        // SET VALIDATION RULES
        $this->form_validation->set_rules('user_name', 'username', 'required');
        $this->form_validation->set_rules('user_pass', 'password', 'required');
        $this->form_validation->set_error_delimiters('<em>', '</em>');
        // has the form been submitted and with valid form info (not empty values)

        if ($this->form_validation->run()) {
            $user_name = $this->input->post('user_name');
            $user_pass = $this->input->post('user_pass');

            //Loads Adminlogin Model file
            $this->load->model('login');
            //query the database
            $result = $this->login->logincheck($user_name, $user_pass);

          
            if ($result) {
                //$sess_array = array();
                foreach ($result as $key => $val) {
                    $sess_array[$key] = $val;
                }
//                print_r($sess_array);die();
                $this->session->set_userdata('agency_admin', $sess_array);

                // user has been logged in
                redirect('dashboard', 'refresh');
            } else {
                $this->session->set_flashdata('message', 'Invalid username or password');
                redirect('adminlogin', 'refresh');
            }
        } else {
//            echo "view";die();
           $this->load->view('adminlogin', $this->data);
        }


        //Loads the Admin Login view
        
    }
    
     public function forgotpassword(){ 

        $this->load->model('common');
         $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];
        
       $email  = $this->input->post('email');
       // print_r($email); die;

       $agency_email=$this->common->select_database_id('agency','agency_email',$email,'agency_email,agency_password,agency_name,agency_password');

       // echo '<pre>'; print_r(count($agency_email)); die;
        if(count($agency_email) > 0)
        {
                $app_name = $agency_email[0]['agency_name'];
                $site_url = $agency_email[0]['agency_email'];
                $app_mail = $agency_email[0]['agency_email'];
                $agpassword = $agency_email[0]['agency_password'];

                //new password
                /*$new_password=rand(1000,999999);

                //update password in database
                $pass_data=array('agency_password'=> $new_password);
                $pass_result=$this->common->update_data($pass_data,'agency','agency_email',$agency_email[0]['agency_email']);

                 // $pass_result=$this->common->update_data($pass_data,'agency_setting','agency_email',$agency_email[0]['agency_email']);
                if(!$pass_result){

                    $this->session->set_flashdata('error','Error Occurred. Try Again.');
                    redirect(site_url().'adminlogin','refresh');
                }*/

        //                    echo $new_password;die();
                $subject = ("rest : Recovery Password");


                $mail_body = "Username is : ".$agency_email[0]['agency_email']."<br> Password is :".$agpassword;
                //Sending mail to admin
        //        $this->email->send();
                if($this->sendEmail($app_name, $app_mail, $agency_email[0]['agency_email'], $subject, $mail_body))
                {
                    $this->session->set_flashdata('success', 'Your password has been emailed to you!.');
                    redirect('adminlogin', 'refresh'); 
                }
                else
                {
                    $this->session->set_flashdata('message', 'Email not sent.');
                    redirect('adminlogin', 'refresh'); 
                }

                
        }
        else
        {
            $this->session->set_flashdata('message', 'Please provide valid email.');
                redirect('adminlogin', 'refresh'); 
        }

        
    }
    
    function sendEmail($app_name='',$app_email='',$to_email='',$subject='',$mail_body='')
    {
      
          $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.zoho.com',
            'smtp_port' => '465',
            'smtp_user' => "kundan.sahani@ashapurasoftech.com", 
            'smtp_pass' => "Aspl@kpsahani8", // change it to yours
            'mailtype'  => 'html', 
            'charset'   => 'utf8'
        );
     

        $this->config->load('email', TRUE);
        $this->cnfemail = $this->config->item('email');

        //Loading E-mail Class
        $this->load->library('email');
        $this->email->initialize($this->cnfemail);
        
        $this->email->from("kundan.sahani@ashapurasoftech.com",$app_name);
        
        $this->email->to($to_email);
        
        $this->email->subject($subject);
            
        $this->email->message("<table border='0' cellpadding='0' cellspacing='0'><tr><td></td></tr><tr><td>" . $mail_body . "</td></tr></table>");
        $this->email->send();
        return;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */