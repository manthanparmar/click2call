<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Agencysettings extends CI_Model {

    

  
    //Getting setting value for editing By id
    function get_setting_value($settingid) {
        $query = $this->db->get_where('setting', array('settingid' => $settingid));

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return nl2br(stripslashes($result[0]['settingvalue']));
        } else {
            return false;
        }
    }

    //Getting setting value for editing By id
    function get_setting_byid($settingid) {
        $query = $this->db->get_where('setting', array('settingid' => $settingid));

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function get_seo_byid($settingid) {
        $query = $this->db->get_where('seo', array('settingid' => $settingid));

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    //Updating Record
    function update($settingid, $value) {
        $data = array('value' => $value);
        $this->db->where('settingid', $settingid);

        if ($this->db->update('setting', $data)) {
            return true;
        } else {
            return false;
        }
    }

    function update_data($data, $tablename, $fieldname, $fieldvalue) {
        $this->db->where($fieldname, $fieldvalue);
        if ($this->db->update($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    function update_data_array($data, $tablename, $field_array) {
        $this->db->where($field_array);
        if ($this->db->update($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    //deleting record
    function delete_data($tablename, $fieldname, $fieldvalue) {
        $this->db->where($fieldname, $fieldvalue);
        if ($this->db->delete($tablename)) {
            return true;
        } else {
            return false;
        }
    }

    //inserting record
    function insert_data($data, $tablename) {
        if ($this->db->insert($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

}

?>
