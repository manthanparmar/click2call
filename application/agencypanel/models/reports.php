<?php

Class Reports extends CI_Model {

    //get all report
    function getAlladvertise($sortby = 'sub_id', $orderby = 'DESC') {

        //Executing Query
        $this->db->from('report');
        $this->db->where('sub_active !=', 'Delete');
        $this->db->order_by($sortby, $orderby);
        $query = $this->db->get();
        return $query->result_array();
    }

    //Getting Page value for editing
    function get_report_byid($sub_id) {


        $this->db->select('*');

        $this->db->from('report');

        $this->db->where('sub_id', $sub_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

}

?>
