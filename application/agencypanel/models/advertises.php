<?php

Class Advertises extends CI_Model {

    //get all advertise
    function getAlladvertise($sortby = 'add_id', $orderby = 'DESC') {

        //Executing Query
        $this->db->from('advertise');
        $this->db->where('add_active !=', 'Delete');
        $this->db->order_by($sortby, $orderby);
        $query = $this->db->get();
        return $query->result_array();
    }

    //Getting Page value for editing
    function get_advertise_byid($add_id) {


        $this->db->select('*');

        $this->db->from('advertise');

        $this->db->where('add_id', $add_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

}

?>
