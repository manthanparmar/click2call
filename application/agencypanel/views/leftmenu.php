<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="<?php echo (($this->uri->segment(1) == 'dashboard')?'active':''); ?>">
                <a title="Dashboard" href="<?php echo base_url('dashboard'); ?>">
                  <i class="fa fa-dashboard"></i> <span>Dashboard</span> <!--<small class="label pull-right bg-green">new</small>-->
                </a>
            </li>

            
            <li class="<?php echo (($this->uri->segment(1) == 'agencysetting')?'active':''); ?>">
                <a href="<?php echo base_url('agencysetting'); ?>" title="Client Settings">
                  <i class="fa fa-cog"></i> <span>Client Settings</span> <!--<small class="label pull-right bg-green">new</small>-->
                </a>
            </li>
            
           <!--<li class="<?php echo (($this->uri->segment(1) == 'emailformat')?'active':''); ?>">
                <a href="<?php echo base_url('emailformat'); ?>" title="Email Format">
                  <i class="fa fa-envelope"></i> <span>Email Format</span>
                </a>
            </li> -->
      
<!--            <li class="<?php echo (($this->uri->segment(1) == 'user')?'active':''); ?>">
                <a href="#" title="User">
                    <i class="fa fa-users"></i> <span>User</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo (($this->uri->segment(1) == 'user') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add user" href="<?php echo base_url('user/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>
                    <li class="<?php echo (($this->uri->segment(1) == 'user') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="User" href="<?php echo base_url('user'); ?>"><i class="fa fa-list"></i>List</a></li>
                </ul>
            </li>-->
   
         


             <li class="<?php echo (($this->uri->segment(1) == 'templatenumber')?'active':''); ?>">
                <a href="#" title="Template Number">
                    <i class="fa fa-phone"></i> <span>Template Number</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo (($this->uri->segment(1) == 'templatenumber') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add templatenumber" href="<?php echo base_url('templatenumber/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>
                    <li class="<?php echo (($this->uri->segment(1) == 'templatenumber') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="templatenumber" href="<?php echo base_url('templatenumber'); ?>"><i class="fa fa-list"></i>List</a></li>
                </ul>
            </li>


      
            
        </ul>
    </section>
</aside>
