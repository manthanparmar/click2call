<?php echo $header; ?>
<?php echo $leftmenu; ?>
<style>
    .btn-active { background: rgba(134, 124, 124, 0.89) none repeat scroll 0 0 !important;}
    .btn-active_history { background: rgba(134, 124, 124, 0.89) none repeat scroll 0 0 !important;}
</style>

<!-- data tables css -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css" />

<!-- Date picker -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 916px !important">
    <section class="content-header">
        <!--        <h1><?php echo $title; ?>
                    <small>Control panel</small>
                </h1>-->
    </section>
    <section class="content">
        <?php

            $payout_usdsum = 0;
            $payout_gbpsum = 0;
            $payout_eurosum = 0;

            foreach ($filtered_data as $filt_data) {    
                if($filt_data['currency'] == 'USD')
                {
                    $payout_usdsum+= $filt_data['payout'];
                }
                if($filt_data['currency'] == 'GBP')
                {
                    $payout_gbpsum+= $filt_data['payout'];
                }
                if($filt_data['currency'] == 'EURO')
                {
                    $payout_eurosum+= $filt_data['payout'];
                }
            }   

        if ($this->session->flashdata('success'))
        {
            ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $this->session->flashdata('success'); ?></strong></div>
        <?php } ?>
        <?php
        if ($this->session->flashdata('error'))
        {
            ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><?php echo $this->session->flashdata('error'); ?></strong></div>
        <?php } ?>

        <div class="row">
           <!-- <section class="col-lg-4 connectedSortable ui-sortable">
                <div class="box box-danger ">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">Navigation</h3>
                        <div class="box-tools pull-right">
                            <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                            </button>

                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="row">  
                            <div class="col-lg-6 col-xs-6">
                                
                                <div class="small-box bg-maroon-gradient">
                                    <div class="inner">
                                        <h3><?php //echo $total; ?></h3>
                                        <p>Template number</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <a href="<?php// echo base_url();?>templatenumber" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </section>-->
            <section class="col-lg-6 connectedSortable">
                <!-- Bar chart -->
                
                    <!-- Bar chart -->
                <div class="box box-primary ">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title"> Visitors Statistics</h3>
                         
                        <div class="box-tools pull-right">
                            <select class="dashboard-select" onchange="get_graph(this.value)" id="click_template">
                                  <option value="All">All Templates</option>
                                  <?php 
                                    foreach($templates as $template){ ?>
                                        <option value="<?php echo $template['template_id'];?>"><?php echo $template['template_name'];?></option>
                                  <?php } ?>
                              </select>
                            <button class="btn btn-small bar_graph" id="bar_week" type="button" style="color: black" onclick="get_click_data('week')">Week</button>
                            <button class="btn btn-small bar_graph btn-active" id="bar_month" type="button" style="color: black" onclick="get_click_data('month')">Month</button>
                            <button class="btn btn-small bar_graph " type="button" id="bar_year" style="color: black" onclick="get_click_data('year')">Year</button>
                            
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                       
                    </div>
                    <div class="box-body">
                        <div id="bar-chart" style="height: 300px;"></div>
                    </div><!-- /.box-body-->
                </div><!-- /.box -->

            </section>
            
            
            <!--call-sms history chart-->
            <section class="col-lg-6 connectedSortable">
                <!-- Bar chart -->
                
                    <!-- Bar chart -->
                <div class="box box-primary ">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title"> Call-Sms Statistics</h3>
                         
                        <div class="box-tools pull-right">
                            <select class="dashboard-history-select" onchange="get_history_graph(this.value)" id="call_history">
                                  <option value="CALL">CALL</option>
                                  <option value="SMS">SMS</option>
                              </select>
                            <button class="btn btn-small bar_history_graph" id="bar_history_week" type="button" style="color: black" onclick="get_history_data('week')">Week</button>
                            <button class="btn btn-small bar_history_graph btn-active_history" id="bar_history_month" type="button" style="color: black" onclick="get_history_data('month')">Month</button>
                            <button class="btn btn-small bar_history_graph " type="button" id="bar_history_year" style="color: black" onclick="get_history_data('year')">Year</button>
                            
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                       
                    </div>
                    <div class="box-body">
                        <div id="bar-chart-history" style="height: 300px;"></div>
                    </div><!-- /.box-body-->
                </div><!-- /.box -->

            </section>






             <!-- Table with filter -->
            <section class="col-lg-12 connectedSortable">
                    <div class="row">

                <section class="col-lg-12 connectedSortable ui-sortable">
                <div class="box box-danger ">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">Navigation</h3>
                        <div class="box-tools pull-right">
                            <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                            </button>

                        </div>
                    </div>
                    
                    
                    
                    <div class="col-md-12" style="margin-bottom: 2%">
                            <form id="filterform" method="post" action="<?php echo base_url();?>dashboard/get_filtered_data">
                            <div class="col-md-3">
                             <label>Select From Date: </label>
                                <div id="datepicker_from" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input class="form-control" value="<?php echo $from_date ?>" name="from_date" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                        
                            <div class="col-md-3">
                              <label>Select To Date: </label>
                                <div id="datepicker_to" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input class="form-control" value="<?php echo $to_date ?>" name="to_date" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                        
                            <div class="col-md-3">
                            <label>Select Type: </label>
                             <select class="form-control" name="calltype" id='calltype'>
                               <option value=''>-- Select Type--</option>
                               <option value='CALL'<?php if($calltype== "CALL") echo "selected"; ?>>CALL</option>
                               <option value='SMS'<?php if($calltype== "SMS") echo "selected"; ?>>SMS</option>
                             </select>
                            </div>

                            <div class="col-md-3">
                                <label></label>
                                <input type="submit" name = "submit" value="Submit" class="btn btn-primary form-control">
                            </div>
                            </form>
                        </div>
                        
                    
                    
                    
                    <div class="box-body border-radius-none">
                        <div class="row">   
                         <div class="col-lg-12 col-xs-12">
                            <div class="col-lg-4 col-xs-12">
                                <div class="small-box bg-yellow-gradient">
                                    <div class="inner">
                                        <h3><?php echo $payout_usdsum; ?></h3>
                                        <p>Total USD</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-usd"></i>
                                    </div>
                                   <!--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                                </div>
                            </div> 

                            <div class="col-lg-4 col-xs-12">
                                <div class="small-box bg-blue-gradient">
                                    <div class="inner">
                                        <h3><?php echo $payout_gbpsum; ?></h3>
                                        <p>Total GBP </p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-gbp"></i>
                                    </div>
                                   <!--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                                </div>
                            </div>


                            <div class="col-lg-4 col-xs-12">
                                <div class="small-box bg-green-gradient">
                                    <div class="inner">
                                        <h3><?php echo $payout_eurosum; ?></h3>
                                        <p>Total EURO </p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-eur"></i>
                                    </div>
                                   <!--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                                </div>
                            </div>

                      </div>
                    </div>
                </div>
              
            </section> 

                    </div>

                    <table id="filter_datatable" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>FROM NUMBER</th>
                            <th>TO NUMBER</th>
                            <th>HISTORY DATE</th>
                            <th>PAYOUT</th>
                            <th>CURRENCY</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            
                            foreach ($filtered_data as $filt_data) { 
                                ?>
                            <tr> 
                                <td><?php echo $filt_data['from_number'];?></td>       
                                <td><?php echo $filt_data['to_number'];?></td> <td><?php echo $filt_data['history_date'];?></td>
                                <td><?php echo $filt_data['payout'];?></td>
                                <td><?php echo $filt_data['currency'];?></td> 
                            </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
             
            </section>



        </div>
    </section>

</div>
</section>
<?php echo $footer; ?> 
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
                                $.widget.bridge('uibutton', $.ui.button);
</script>
<script type="text/javascript">
    $(document).ready(function () {
        get_click_data('week'); // load user graph on load 
        get_history_data('week');
    });
</script>

<script type="text/javascript">
$(function () {
  $("#datepicker_from").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update','<?php echo $from_date;?>');

  $("#datepicker_to").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update','<?php echo $to_date;?>');

});
</script>


<script type="text/javascript">
    $(document).ready(function() {
    $('#filter_datatable').DataTable().destroy();
    $('#filter_datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [
             { extend: 'excel', className: 'btn-primary form-control' }
        ]
    });
} );
</script>


<!-- Date picker js -->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.print.min.js"></script>


<script type="text/javascript">

function get_graph(click_template)
{
    var type = $('.btn-active').html().toLowerCase();
    
    if (type != "")
        {
            var w;
            $.ajax({url: '<?php echo base_url() . 'dashboard/get_click_data' ?>',
                data: {type: type,click_template:click_template},
//                    dataType: 'json',
                type: 'post',
                success: function (data) {
                    var d = JSON.parse(data);
                    $('.bar_graph').removeClass('btn-active');
                    $('#bar_' + type).addClass('btn-active');
                    $("#bar-chart").html('');


                    //BAR CHART
                    var bar = new Morris.Bar({
                        element: 'bar-chart',
                        resize: true,
                        data: d,
                        barColors: ['#3C8DBC'],
                        xkey: 'Date',
                        ykeys: ['Visitors'],
                        labels: ['Visitors'],
                        hideHover: 'auto'
                    });

                }
            });
        }
}

    function get_click_data(type)
    {
        var click_template = $('#click_template').val();
        if (type != "")
        {
            var w;
            $.ajax({url: '<?php echo base_url() . 'dashboard/get_click_data' ?>',
                data: {type: type,click_template:click_template},
//                    dataType: 'json',
                type: 'post',
                success: function (data) {
                    var d = JSON.parse(data);
                    $('.bar_graph').removeClass('btn-active');
                    $('#bar_' + type).addClass('btn-active');
                    $("#bar-chart").html('');


                    //BAR CHART
                    var bar = new Morris.Bar({
                        element: 'bar-chart',
                        resize: true,
                        data: d,
                        barColors: ['#3C8DBC'],
                        xkey: 'Date',
                        ykeys: ['Visitors'],
                        labels: ['Visitors'],
                        hideHover: 'auto'
                    });

                }
            });
        }
    }





function get_history_graph(call_history)
{
    var type = $('.btn-active_history').html().toLowerCase();
    // alert(call_history);
    if (type != "")
        {
            var w;
            $.ajax({url: '<?php echo base_url() . 'dashboard/get_callhistory_data' ?>',
                data: {type: type,call_history:call_history},
//                    dataType: 'json',
                type: 'post',
                success: function (data) {
                    var d = JSON.parse(data);
                    $('.bar_history_graph').removeClass('btn-active_history');
                    $('#bar_history_' + type).addClass('btn-active_history');
                    $("#bar-chart-history").html('');


                    //BAR CHART
                    var bar = new Morris.Bar({
                        element: 'bar-chart-history',
                        resize: true,
                        data: d,
                        barColors: ['#605ca8'],
                        xkey: 'Date',
                        ykeys: [call_history],
                        labels: [call_history],
                        hideHover: 'auto'
                    });

                }
            });
        }
}


 function get_history_data(type)
    {
        var call_history = $('#call_history').val();
        if (type != "")
        {
            var w;
            $.ajax({url: '<?php echo base_url() . 'dashboard/get_callhistory_data' ?>',
                data: {type: type,call_history:call_history},
//                    dataType: 'json',
                type: 'post',
                success: function (data) {
                    var d = JSON.parse(data);
                   $('.bar_history_graph').removeClass('btn-active_history');
                    $('#bar_history_' + type).addClass('btn-active_history');
                    $("#bar-chart-history").html('');


                    //BAR CHART
                    var bar = new Morris.Bar({
                        element: 'bar-chart-history',
                        resize: true,
                        data: d,
                        barColors: ['#605ca8'],
                        xkey: 'Date',
                        ykeys: [call_history],
                        labels: [call_history],
                        hideHover: 'auto'
                    });

                }
            });
        }
    }
  
</script>
<!--	end of footer	-->


