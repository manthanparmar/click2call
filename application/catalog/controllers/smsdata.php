<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class smsdata extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/templatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();

        // include('include.php');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'getsmsdata';
        $this->data['section_title'] = 'getsmsdata';
        $this->data['site_name'] = 'SMS-DATA';
        $this->data['site_url'] = 'SMS-DATA';

//Load leftsidemenu and save in variable

        $this->load->library('upload');
        $this->load->model('common');
   
    }

    public function index()
    {
        $this->load->view('404');
    }

    public function get_sms_data()
    {
        //Fetch Agency APIUsername and APIpassword
        $agencydata = $this->common->get_agency_data_all('agency');
            
         //echo "<pre>"; print_r($agencydata); die;
        $totalagency = count($agencydata);
        
        for($i=0;$i<$totalagency;$i++)
        {
            $ch = curl_init();
            $APIUSERNAME = $agencydata[$i]['api_username'];
            $APIPASSWORD = $agencydata[$i]['api_password'];
            
            $enddate = date("Y-m-d");
            $startdate = date('Y-m-d', strtotime('-7 days', strtotime($enddate)));
            $username = "TESTSMS";
            $password = "TEST123";
         /*   $username = $APIUSERNAME;
            $password = $APIPASSWORD;
             */
            // print_r($password); die;
            // $url = "http://api.telcoitlimited.com/SmsInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionSmsInboundAllocationSearch[date_range]=$startdate+00:00+-+$enddate+23:59";            
            curl_setopt($ch, CURLOPT_URL, "http://api.telcoitlimited.com/SmsInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionSmsInboundAllocationSearch[date_range]=$startdate+00:00+-+$enddate+23:59&per-page=100");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            
            $result = curl_exec($ch);
        
            if (curl_errno($ch)) {
               echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);
            $result = json_decode($result);
             
            $history_unique_id = "";
            $tonumber = "";
            $fromnumber = "";
            $history_type = "SMS";
            $history_date = "";
    

            if(count($result) > 0)
            {
              if(isset($result->status) && $result->status == '401')
              {
                  continue;
              }
              else
              {
                foreach ($result as $row) 
                {
                     // echo "<pre>"; print_r($row);  die;
        
                    $history_unique_id = $row->id;
        
                    $tonumber = $row->destination_addr;
        
                    $fromnumber = $row->source_addr;
        
                    $history_type = 'SMS';
        
                    $history_date = $row->start_stamp;
          
                    $number = $this->get_number_id($tonumber);
                 
                    // print_r($history_unique_id); die;
                //check number already function
                    
                    // echo "<pre>"; print_r(count($number)); die;
                    if(count($number) > 0)
                    {
                        $data = array('calltype'=>'SMS');
                        $this->common->update_data($data, 'number', 'number', $number->number);
                        
                        $numberid = $number->number_id;
                        $agency = $this->get_agency_id($numberid);
                        // print_r($agency); die;
                        if(count($agency) > 0)
                        {
                             $agencyid = $agency->agency_id;
        
                        $this->data['existid'] = $this->common->select_database_id('call_sms_history', 'history_unique_id', $history_unique_id, $data = '*');
        
                         // print_r(count($this->data['existid'])); die;
        
                          if(count($this->data['existid']) > 0)
                          {
                            //do nothing
                          }
                          else
                          {
        
                            // print_r('hello'); die;
                              $sms_history_data = array(
        
                                  'history_unique_id' => $history_unique_id,
                                  'to_number' => $tonumber,
                                  'from_number' => $fromnumber,
                                  'from_numberid' => $numberid,
                                  'history_date' => $history_date,
                                  'createddate' => date('Y-m-d H:i:s'),
                                  'agency_id' => $agencyid,
                                  'history_type' => 'SMS'
                              );
        
                           // echo "<pre>"; print_r($sms_history_data); die;
                              //echo "<pre>"; print_r($sms_history_data);
                              $this->db->insert('call_sms_history',$sms_history_data);
        
                              $this->dt['Message'] = 'SMS data insert successfully';
                              
                          }
                        }
                        else
                        {
                          $this->dt['Message'] = 'Agency not matched';
                        }
                       
                    }
                    else
                    {
                        $this->dt['Message'] = 'Number not matched';
                       // do nothing
                    }
                    
                } 
              }
                     
            }
            else
            {
                 $this->dt['Message'] = 'No record found';
                 continue;
            }
            echo json_encode($this->dt);
        }
        die;
        
    }

    public function get_number_id($fromnumber)
    {
        $result = $this->common->get_numberid($fromnumber);
        return $result;
    }

    public function get_agency_id($numberid)
    {
        $result = $this->common->get_agencyid($numberid);
        return $result;
    }

}

