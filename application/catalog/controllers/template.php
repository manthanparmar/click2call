<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class template extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/templatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();

        // include('include.php');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'Template';
        $this->data['section_title'] = 'Template';
        $this->data['site_name'] = 'Template';
        $this->data['site_url'] = 'Template';

//Load leftsidemenu and save in variable


        $this->load->library('upload');
        $this->load->model('common');
        $this->load->model('previewtemplates');
        
        
      /*  $this->data['template_data'] = $this->previewtemplates->get_template_id_from_shortcode($code);
            
        $templateid = $this->data['template_data'][0]['template_id'];
        $agencyid = $this->data['template_data'][0]['agency_id'];
        
        $urlclick_data = array(
            'agency_id' => $agencyid,
            'template_id' => $templateid,
            'createddate'=> date('Y-m-d H:i:s'),
            'click'=>'YES'
        );
        
        $this->common->insert_data($urlclick_data, 'url_click'); */
    }


    public function index($code='')
    {
        if($code == NULL)
        {
            $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
            $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
            $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

            if( $iPod || $iPhone ){
                echo "browser reported as an iPhone/iPod touch -- do something here";
            }else if($iPad){
                echo "browser reported as an iPad -- do something here";
            }else if($Android){
                echo "browser reported as an Android device -- do something here";
            }else if($webOS){
                echo "browser reported as a webOS device -- do something here";
            }
            else
            {
                echo "else";
            }
            die;
            $this->load->view('404');        
        }
        else
        {
            $this->data['template_data'] = $this->previewtemplates->get_template_id_from_shortcode($code);
             // echo "<pre>";print_r($this->data['template_data']); die;
            $templateid = $this->data['template_data'][0]['template_id'];
            $agencyid = $this->data['template_data'][0]['agency_id'];
            $actid = $this->data['template_data'][0]['actid'];
           
            /*print_r($actid); die;*/
            $this->data['templatedata'] = $this->previewtemplates->get_template_preview($templateid);
            $this->data['numberdata'] = $this->previewtemplates->get_number_from_template_number($agencyid,$templateid,$actid);
            $this->data['smsnumberdata'] = $this->previewtemplates->get_smsnumber_from_template_number($agencyid,$templateid,$actid);
             // echo "<pre>";print_r($this->data['smsnumberdata']); die;
            $templateview = '';
            $templateview = $this->data['templatedata'][0]['template_view'];
         
            $gifpath = $this->config->item('gif_path');
            $url = base_url().'/'.$gifpath;
            //Click data insert in different table
            
            $urlclick_data = array(
                'agency_id' => $agencyid,
                'template_id' => $templateid,
                'createddate'=> date('Y-m-d H:i:s'),
                'click'=>'YES'
            );
            
            $this->common->insert_data($urlclick_data, 'url_click'); 
            
            
            $i=1;
            
            $countdata = count($this->data['numberdata']);
            
            foreach($this->data['numberdata'] as $numbers)
            {
                $replacenumber = 'tel:'.$numbers['number'];
                $templateview = str_replace("%number$i%",$replacenumber,$templateview);
                $templateview = str_replace("%url%",$url,$templateview);
                $i++;
            }
            $k = 1;
            foreach($this->data['smsnumberdata'] as $smsnumbers)
            {
                $replacenumber = 'sms:'.$smsnumbers['number'];
                $templateview = str_replace("%smsnumber$k%",$replacenumber,$templateview);
                $templateview = str_replace("%url%",$url,$templateview);
                $k++;
            }

            for($j=$countdata+1;$j<100;$j++)
            {
                $templateview = str_replace("%number$j%",'javascript:void(0)',$templateview);
                $templateview = str_replace("%smsnumber$j%",'javascript:void(0)',$templateview);
            }
            
            $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
            $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
            $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

            if( $iPod)
            {
                $templateview = str_replace("?body=","&body=",$templateview);
            }
            else if($iPhone)
            {
                $templateview = str_replace("?body=","&body=",$templateview);
            }
            else if($iPad)
            {
                $templateview = str_replace("?body=","&body=",$templateview);
            }
            else if($Android)
            {
                $templateview = str_replace("?body=","?body=",$templateview);
            }
            else if($webOS)
            {
               $templateview = str_replace("?body=","?body=",$templateview);
            }
           


           $this->data['temp_data'] = $templateview;
          
           $this->load->view('image',$this->data);
        }
        
    }
    
   /* public function shorturl($shortcode = NULL)
    {
        
        if ($shortcode == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('template', 'refresh');
        }
        else
        {
            
            $this->data['template_data'] = $this->previewtemplates->get_template_id_from_shortcode($shortcode);
            
            $templateid = $this->data['template_data'][0]['template_id'];
            $agencyid = $this->data['template_data'][0]['agency_id'];
            
            $this->data['templatedata'] = $this->previewtemplates->get_template_preview($templateid);
            $this->data['numberdata'] = $this->previewtemplates->get_number_from_template_number($agencyid,$templateid);
            $templateview = '';
            $templateview = $this->data['templatedata'][0]['template_view'];
         
            $i=1;
            
            $countdata = count($this->data['numberdata']);
            
            foreach($this->data['numberdata'] as $numbers)
            {
                if($numbers['calltype'] == 'SMS')
                {
                    $replacenumber = 'sms:'.$numbers['number'];
                }
                else if($numbers['calltype'] == 'CALL')
                {
                    $replacenumber = 'tel:'.$numbers['number'];
                }
                else
                {
                    $replacenumber = 'manthan';
                }
               $templateview = str_replace("%number$i%",$replacenumber,$templateview);
               $i++;
            }
            for($j=$countdata+1;$j<100;$j++)
            {
                $templateview = str_replace("%number$j%",'/#',$templateview);
            }
            print_r($templateview); 
            die; 
        }
    }
        */
    
    public function preview($templateid = NULL,$agencyid = NULL,$actid = NULL)
    {
        if ($templateid == NULL || $agencyid == NULL || $actid == NULL)
        {
            $this->load->view('404');
        }
        else
        {
            $templateid = base64_decode($templateid);
            $agencyid = base64_decode($agencyid);
            $actid = base64_decode($actid);
            
            $this->data['templatedata'] = $this->previewtemplates->get_template_preview($templateid);
            $this->data['numberdata'] = $this->previewtemplates->get_number_from_template_number($agencyid,$templateid,$actid);
            $this->data['smsnumberdata'] = $this->previewtemplates->get_smsnumber_from_template_number($agencyid,$templateid,$actid);
            $templateview = '';
            $templateview = $this->data['templatedata'][0]['template_view'];
         
            $gifpath = $this->config->item('gif_path');
            $url = base_url().'/'.$gifpath;
         
            //Click data insert in different table
            
            $urlclick_data = array(
                'agency_id' => $agencyid,
                'template_id' => $templateid,
                'createddate'=> date('Y-m-d H:i:s'),
                'click'=>'YES'
            );
            
            $this->common->insert_data($urlclick_data, 'url_click'); 
            
           
            $i=1;
            
            $countdata = count($this->data['numberdata']);
            
            foreach($this->data['numberdata'] as $numbers)
            {
                $replacenumber = 'tel:'.$numbers['number'];
                $templateview = str_replace("%number$i%",$replacenumber,$templateview);
                $templateview = str_replace("%url%",$url,$templateview);
                $i++;
            }
            $k = 1;
            foreach($this->data['smsnumberdata'] as $smsnumbers)
            {
                $replacenumber = 'sms:'.$smsnumbers['number'];
                $templateview = str_replace("%smsnumber$k%",$replacenumber,$templateview);
                $templateview = str_replace("%url%",$url,$templateview);
                $k++;
            }
            for($j=$countdata+1;$j<100;$j++)
            {
                $templateview = str_replace("%number$j%",'javascript:void(0)',$templateview);
                $templateview = str_replace("%smsnumber$j%",'javascript:void(0)',$templateview);
            }

            $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
            $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
            $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

            if( $iPod)
            {
                $templateview = str_replace("?body=","&body=",$templateview);
            }
            else if($iPhone)
            {
                $templateview = str_replace("?body=","&body=",$templateview);
            }
            else if($iPad)
            {
                $templateview = str_replace("?body=","&body=",$templateview);
            }
            else if($Android)
            {
                $templateview = str_replace("?body=","?body=",$templateview);
            }
            else if($webOS)
            {
               $templateview = str_replace("?body=","?body=",$templateview);
            }
            
            print_r($templateview); 
            die;
            
            //Dont remove below line it is return html view
            //print_r($this->data['templatedata'][0]['template_view']); die;
        }
    }
     
}

