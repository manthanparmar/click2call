<?php

Class previewtemplates extends CI_Model {

    public function get_template_preview($templateid)
    {
        return $this->db->query("SELECT * FROM `template` WHERE template_id = $templateid")->result_array();
    }
    
    public function get_number_from_template_number($agencyid,$templateid,$actid)
    {
        return $this->db->query("
            SELECT template_number.*,number.number,number.calltype 
            FROM `template_number` 
            JOIN number ON template_number.number_id = number.number_id 
            WHERE template_number.template_id = $templateid 
            AND template_number.agency_id = $agencyid 
            AND template_number.act_id = $actid 
            AND template_number.type = 'CALL'")->result_array();
    }
    
    public function get_smsnumber_from_template_number($agencyid,$templateid,$actid)
    {
        return $this->db->query("
            SELECT template_number.*,number.number,number.calltype 
            FROM `template_number` 
            JOIN number ON template_number.number_id = number.number_id 
            WHERE template_number.template_id = $templateid 
            AND template_number.agency_id = $agencyid 
            AND template_number.act_id = $actid 
            AND template_number.type = 'SMS'")->result_array();
    }
      public function get_template_id_from_shortcode($shortcode)
    {
        // return $this->db->query("SELECT * FROM `shorten_url_code` WHERE code = '$shortcode'")->result_array();

        return $this->db->query("SELECT * FROM `agency_created_template` WHERE code = '$shortcode'")->result_array();
    }

}
